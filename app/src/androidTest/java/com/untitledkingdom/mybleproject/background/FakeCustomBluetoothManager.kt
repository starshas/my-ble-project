package com.untitledkingdom.mybleproject.background

import com.juul.kable.Advertisement
import com.juul.kable.State
import com.untitledkingdom.mybleproject.bluetooth.CustomBluetoothManager
import com.untitledkingdom.mybleproject.bluetooth.data.ReadingsData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.emptyFlow
import java.time.LocalDate
import javax.inject.Inject

internal class FakeCustomBluetoothManager @Inject constructor() : CustomBluetoothManager {
    override val canUseBluetooth: Boolean
        get() = true
    override val peripheralExists: Boolean
        get() = true
    override val deviceAddress: String
        get() = "deviceAddress"
    override val stateFlow: StateFlow<State>
        get() = MutableStateFlow(State.Connected)
    override val isConnected: Boolean
        get() = true

    @FlowPreview
    override val flowReadings: Flow<ReadingsData>
        get() = emptyFlow()
    override val isConnecting: Boolean
        get() = false
    override val isBluetoothEnabled: Boolean
        get() = true
    override val hasBluetoothPermission: Boolean
        get() = true
    override val getDeviceNameOrAddress: String
        get() = "DeviceNameOrAddress"

    @FlowPreview
    override val flowBatteryLevel: Flow<Short> = emptyFlow()

    override fun turnOnBluetooth() = Unit
    override fun turnOffBluetooth() = Unit

    override suspend fun connect(
        scope: CoroutineScope,
        advertisement: Advertisement,
        callbackConnected: () -> Unit,
        callbackError: (Exception, String) -> Unit
    ) = Unit

    override suspend fun connect(
        scope: CoroutineScope,
        identifier: String,
        callbackConnected: () -> Unit,
        callbackError: (Exception, String) -> Unit
    ) = Unit

    override suspend fun disconnect(): Unit = Unit
    override suspend fun getRssi(): Int = 39
    override fun getDeviceName(): String = "DeviceName"
    override suspend fun writeReadingsCurrentTime() = Unit
    override suspend fun reconnect(callbackError: (Exception, String) -> Unit): Unit = Unit
    override suspend fun getReadingsTime(): LocalDate = LocalDate.now()
    override suspend fun readBattery(): Short = 50
}
