package com.untitledkingdom.mybleproject.background

import com.untitledkingdom.mybleproject.bluetooth.CustomBluetoothManager
import com.untitledkingdom.mybleproject.utils.CustomBluetoothManagerModule
import dagger.Binds
import dagger.Module
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn
import kotlinx.coroutines.FlowPreview
import javax.inject.Singleton

@FlowPreview
@Module
@TestInstallIn(
    components = [SingletonComponent::class],
    replaces = [CustomBluetoothManagerModule::class]
)
abstract class FakeCustomBluetoothManagerModule {
    @Singleton
    @Binds
    internal abstract fun bindCustomBluetoothManager(
        fakeCustomBluetoothManager: FakeCustomBluetoothManager
    ): CustomBluetoothManager
}
