package com.untitledkingdom.mybleproject.background

import android.content.Intent
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ServiceTestRule
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.FlowPreview
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.runner.RunWith
import java.util.concurrent.TimeUnit

@FlowPreview
@MediumTest
@RunWith(AndroidJUnit4::class)
@HiltAndroidTest
internal class ForegroundServiceTest {
    @get:Rule
    val serviceRule: ServiceTestRule = ServiceTestRule.withTimeout(20L, TimeUnit.SECONDS)

    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @Before
    fun setUp() = hiltRule.inject()

    @Test
    fun checkInitializationWithFakeCustomBluetoothManager() {
        val intent = Intent(
            InstrumentationRegistry.getInstrumentation().targetContext,
            ForegroundService::class.java
        )
        serviceRule.startService(intent)
        val binder = serviceRule.bindService(intent) as ForegroundService.LocalBinder
        val usedCustomBluetoothManager = binder.service.customBluetoothManager

        assertTrue(usedCustomBluetoothManager is FakeCustomBluetoothManager)
    }

    @Test
    fun checkReceivedIntentServiceStop() {
        val intent = Intent(
            InstrumentationRegistry.getInstrumentation().targetContext,
            ForegroundService::class.java
        ).apply {
            action = INTENT_ACTION_STOP_SERVICE
        }

        serviceRule.startService(intent)
        val binder = serviceRule.bindService(intent) as ForegroundService.LocalBinder
        val service = binder.service

        assertTrue(service.receivedIntentStopService)
    }

    @Test
    fun checkReceivedIntentStartFromWelcomeScreen() {
        val intent = Intent(
            InstrumentationRegistry.getInstrumentation().targetContext,
            ForegroundService::class.java
        ).apply {
            action = INTENT_ACTION_START_FROM_WELCOME_SCREEN
        }

        serviceRule.startService(intent)
        val binder = serviceRule.bindService(intent) as ForegroundService.LocalBinder
        val service = binder.service

        assertTrue(service.receivedIntentStartFromWelcomeScreen)
    }

    @Test
    fun checkReceivedIntentStartFromMainScreen() {
        val intent = Intent(
            InstrumentationRegistry.getInstrumentation().targetContext,
            ForegroundService::class.java
        ).apply {
            action = INTENT_ACTION_START_FROM_MAIN_SCREEN
        }

        serviceRule.startService(intent)
        val binder = serviceRule.bindService(intent) as ForegroundService.LocalBinder
        val service = binder.service

        assertTrue(service.receivedIntentStartFromMainScreen)
    }
}
