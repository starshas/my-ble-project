package com.untitledkingdom.mybleproject.api

object ApiConstants {
    const val DELAY_REQUESTS_MILLIS = 2000L
    const val HTTP_STATUS_OK = 200
    const val HTTP_STATUS_NO_CONTENT = 204
    const val HTTP_STATUS_NOT_FOUND = 404
    const val NUMBER_OF_RECORDS_IN_SYNC_REQUEST = 20
}
