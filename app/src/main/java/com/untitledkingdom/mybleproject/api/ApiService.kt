package com.untitledkingdom.mybleproject.api

import android.net.ConnectivityManager
import com.untitledkingdom.mybleproject.api.data.ApiReadingsRecord
import retrofit2.Response

interface ApiService {
    suspend fun syncRecords(
        listRecords: List<ApiReadingsRecord>,
        connectivityManager: ConnectivityManager
    ): Response<Unit>
}
