package com.untitledkingdom.mybleproject.api

import android.net.ConnectivityManager
import com.untitledkingdom.mybleproject.api.ApiConstants.HTTP_STATUS_OK
import com.untitledkingdom.mybleproject.api.data.ApiReadingsRecord
import com.untitledkingdom.mybleproject.utils.network.NetworkUtils.isOnline
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody.Companion.toResponseBody
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ApiServiceImpl @Inject constructor() : ApiService {
    override suspend fun syncRecords(
        listRecords: List<ApiReadingsRecord>,
        connectivityManager: ConnectivityManager
    ): Response<Unit> = runBlocking {
        delay(ApiConstants.DELAY_REQUESTS_MILLIS)
        if (connectivityManager.isOnline())
            Response.success(HTTP_STATUS_OK, Unit)
        else
            Response.error(
                ApiConstants.HTTP_STATUS_NOT_FOUND,
                "".toResponseBody(
                    "".toMediaTypeOrNull()
                )
            )
    }
}
