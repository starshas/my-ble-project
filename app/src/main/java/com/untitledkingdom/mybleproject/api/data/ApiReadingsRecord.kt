package com.untitledkingdom.mybleproject.api.data

import java.time.OffsetDateTime

data class ApiReadingsRecord(
    val temperature: Float,
    val humidity: Int,
    val dateAndTime: OffsetDateTime
)
