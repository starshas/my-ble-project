package com.untitledkingdom.mybleproject.background

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Network
import android.os.Binder
import android.os.IBinder
import androidx.annotation.RestrictTo
import androidx.core.app.NotificationCompat
import com.juul.kable.State
import com.untitledkingdom.mybleproject.R
import com.untitledkingdom.mybleproject.api.ApiConstants
import com.untitledkingdom.mybleproject.api.ApiService
import com.untitledkingdom.mybleproject.api.data.ApiReadingsRecord
import com.untitledkingdom.mybleproject.bluetooth.CustomBluetoothManager
import com.untitledkingdom.mybleproject.bluetooth.data.ReadingsData
import com.untitledkingdom.mybleproject.database.AppDatabase
import com.untitledkingdom.mybleproject.database.data.ReadingsRecord
import com.untitledkingdom.mybleproject.datastore.DataStorage
import com.untitledkingdom.mybleproject.utils.network.NetworkUtils.isOnline
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.filterIsInstance
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import timber.log.Timber
import java.time.OffsetDateTime
import javax.inject.Inject

private const val CHANNEL_ID_MAIN = "100"
private const val CHANNEL_ID_LOW_BATTERY = "101"
private const val TAG_NOTIFICATION_LOW_BATTERY = "TAG_NOTIFICATION_LOW_BATTERY"
private const val ID_NOTIFICATION_LOW_BATTERY = 1000
private const val ID_FOREGROUND = 1
const val INTENT_ACTION_STOP_SERVICE = "INTENT_ACTION_STOP_SERVICE"
const val INTENT_ACTION_START_FROM_WELCOME_SCREEN = "INTENT_ACTION_START_FROM_WELCOME"
const val INTENT_ACTION_START_FROM_MAIN_SCREEN = "INTENT_ACTION_START_FROM_MAIN"
private const val BATTERY_LEVEL_NOTIFICATION_PERCENTAGE = 20
private const val DELAY_RECONNECT_MILLIS = 500L

@FlowPreview
@AndroidEntryPoint
class ForegroundService : Service() {
    private var isSyncing = false
    private var isReconnecting = false
    private var stopServiceAfterAllDataSend = false

    @Inject
    lateinit var dataBase: AppDatabase

    @Inject
    lateinit var notificationManager: NotificationManager

    @Inject
    lateinit var customBluetoothManager: CustomBluetoothManager

    @Inject
    lateinit var apiService: ApiService

    @Inject
    lateinit var connectivityManager: ConnectivityManager

    @Inject
    lateinit var dataStorage: DataStorage

    private val coroutineScopeReconnect = CoroutineScope(Dispatchers.IO)
    private val coroutineScopeReadingsAndBattery = CoroutineScope(Dispatchers.IO)
    private val coroutineScopeSync = CoroutineScope(Dispatchers.IO)
    private var isLowBatteryNotificationShown = false
    private val mainDao get() = dataBase.getMainDao()
    private val isOnline get() = connectivityManager.isOnline()

    @RestrictTo(RestrictTo.Scope.TESTS)
    var receivedIntentStartFromWelcomeScreen = false

    @RestrictTo(RestrictTo.Scope.TESTS)
    var receivedIntentStopService = false

    @RestrictTo(RestrictTo.Scope.TESTS)
    var receivedIntentStartFromMainScreen = false

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Timber.d("Service onStartCommand")
        startInternetChecker()

        if (intent == null)
            return START_STICKY

        when (intent.action) {
            INTENT_ACTION_STOP_SERVICE -> stopService()
            INTENT_ACTION_START_FROM_WELCOME_SCREEN -> handleStartedFromWelcomeScreen()
            INTENT_ACTION_START_FROM_MAIN_SCREEN -> handleStartedFromMainScreen()
        }
        return START_STICKY
    }

    override fun onCreate() {
        super.onCreate()
        createForegroundChannel()
        createLowBatteryChannel()
        makeForeground()
    }

    private fun handleStartedFromWelcomeScreen() {
        stopServiceAfterAllDataSend = true
        receivedIntentStartFromWelcomeScreen = true
    }

    private fun handleStartedFromMainScreen() {
        stopServiceAfterAllDataSend = false

        Timber.d("before startCollectingData")
        coroutineScopeReconnect.enableAutoReconnect()
        startCollectingData()
        Timber.d("after startCollectingData")
        receivedIntentStartFromMainScreen = true
    }

    private fun startCollectingData() {
        coroutineScopeReadingsAndBattery.launch {
            Timber.d("flow readings = ${customBluetoothManager.flowReadings}")
            merge(
                getFlowBattery() ?: emptyFlow(),
                getFlowReadings()?.onEach {
                    doSyncIfNeed()
                } ?: emptyFlow()
            ).catch { cause ->
                Timber.d("Service merge error $cause")
            }.collect()
        }
    }

    private fun CoroutineScope.enableAutoReconnect() {
        val stateFlow = customBluetoothManager.stateFlow
        stateFlow?.filterIsInstance<State.Disconnected>()
            ?.onEach {
                while (canDoReconnect(stateFlow)) {
                    doReconnect()
                    delay(DELAY_RECONNECT_MILLIS)
                }
            }
            ?.launchIn(this)
    }

    private fun canDoReconnect(peripheralStateFlow: StateFlow<State>) =
        !isReconnecting && customBluetoothManager.canUseBluetooth && peripheralStateFlow.value is State.Disconnected && customBluetoothManager.peripheralExists

    private suspend fun doReconnect() {
        Timber.d("Doing reconnect ${hashCode()}")
        isReconnecting = true
        customBluetoothManager.reconnect(
            callbackError = { exception, errorMessage ->
                Timber.d(exception, errorMessage)
            }
        )
        isReconnecting = false
    }

    private fun startInternetChecker() {
        Timber.d("Started Internet Checker")
        connectivityManager.registerDefaultNetworkCallback(
            object : ConnectivityManager.NetworkCallback() {
                override fun onAvailable(network: Network) {
                    Timber.d("Connectivity Manager onAvailable")
                    doSyncIfNeed()
                }
            }
        )
    }

    private fun createLowBatteryChannel() {
        val serviceChannel = NotificationChannel(
            CHANNEL_ID_LOW_BATTERY,
            getString(R.string.foreground_service_channel_name_low_battery),
            NotificationManager.IMPORTANCE_LOW
        )
        notificationManager.createNotificationChannel(serviceChannel)
    }

    private fun getFlowBattery(): Flow<Short>? {
        Timber.d("getFlowBatteryLevel() = ${customBluetoothManager.flowBatteryLevel}")
        return customBluetoothManager.flowBatteryLevel?.onEach { batteryLevel ->
            Timber.d("batteryLevel = $batteryLevel")
            isLowBatteryNotificationShown =
                if (batteryLevel < BATTERY_LEVEL_NOTIFICATION_PERCENTAGE) {
                    if (!isLowBatteryNotificationShown)
                        showLowBatteryNotification()
                    true
                } else {
                    hideLowBatteryNotification()
                    false
                }
        }
    }

    private fun hideLowBatteryNotification() =
        notificationManager.cancel(TAG_NOTIFICATION_LOW_BATTERY, ID_NOTIFICATION_LOW_BATTERY)

    private fun showLowBatteryNotification() {
        val notification = NotificationCompat.Builder(this, CHANNEL_ID_LOW_BATTERY)
            .setContentTitle(getString(R.string.foreground_service_low_battery))
            .setSmallIcon(R.mipmap.ic_launcher_round)
            .build()
        notificationManager.notify(
            TAG_NOTIFICATION_LOW_BATTERY,
            ID_NOTIFICATION_LOW_BATTERY,
            notification
        )
    }

    private fun makeForeground() {
        val notification: Notification = NotificationCompat.Builder(this, CHANNEL_ID_MAIN)
            .setContentTitle(getString(R.string.foreground_service_title))
            .setSmallIcon(R.mipmap.ic_launcher_round)
            .build()
        startForeground(ID_FOREGROUND, notification)
    }

    private fun getFlowReadings(): Flow<ReadingsData>? =
        customBluetoothManager.flowReadings?.onEach { readingsData ->
            Timber.d("Service flowReadings collect")
            val readingsRecord = ReadingsRecord(
                temperature = readingsData.temperature,
                humidity = readingsData.humidity,
                dateAndTime = OffsetDateTime.now()
            )
            customBluetoothManager.writeReadingsCurrentTime()
            addRecordToDb(readingsRecord)
        }

    private fun doSyncIfNeed() {
        if (isOnline && !isSyncing) {
            isSyncing = true
            coroutineScopeSync.launch {
                while (isOnline && hasPortionOfUnsyncedRecords()) {
                    Timber.d("Doing sync")
                    doSync(listRecords = mainDao.getPortionOfFirstUnsyncedRecords())
                }
                isSyncing = false
                if (stopServiceAfterAllDataSend) {
                    stopService()
                    stopServiceAfterAllDataSend = false
                }
            }
        }
    }

    private suspend fun hasPortionOfUnsyncedRecords() =
        mainDao.getPortionOfFirstUnsyncedRecords().size == ApiConstants.NUMBER_OF_RECORDS_IN_SYNC_REQUEST

    private suspend fun doSync(listRecords: List<ReadingsRecord>) {
        val apiListReadings = List(listRecords.size) { index ->
            val itemRecord = listRecords[index]
            ApiReadingsRecord(
                temperature = itemRecord.temperature,
                humidity = itemRecord.humidity,
                dateAndTime = itemRecord.dateAndTime
            )
        }
        val response = apiService.syncRecords(
            listRecords = apiListReadings,
            connectivityManager = connectivityManager
        )
        if (response.isSuccessful) {
            Timber.d("Successfully synced $listRecords")
            listRecords.forEach {
                mainDao.addRecord(it.copy(isSynced = true))
            }
        }
    }

    private suspend fun addRecordToDb(readingsRecord: ReadingsRecord) =
        mainDao.addRecord(readingsRecord = readingsRecord)

    private fun stopService() {
        coroutineScopeReadingsAndBattery.cancel()
        coroutineScopeSync.cancel()
        coroutineScopeReconnect.cancel()
        stopSelf()
        receivedIntentStopService = true
    }

    override fun onBind(intent: Intent?): IBinder = LocalBinder()

    inner class LocalBinder : Binder() {
        val service: ForegroundService
            get() = this@ForegroundService
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.d("Service destroyed")
        stopService()
    }

    private fun createForegroundChannel() {
        val serviceChannel = NotificationChannel(
            CHANNEL_ID_MAIN,
            getString(R.string.foreground_service_channel_name),
            NotificationManager.IMPORTANCE_LOW
        )
        notificationManager.createNotificationChannel(serviceChannel)
    }
}
