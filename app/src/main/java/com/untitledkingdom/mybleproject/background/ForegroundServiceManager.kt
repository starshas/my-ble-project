package com.untitledkingdom.mybleproject.background

import android.content.ComponentName

interface ForegroundServiceManager {
    fun startService(fromClass: Class<*>): ComponentName?
    fun stopService(): ComponentName?
}
