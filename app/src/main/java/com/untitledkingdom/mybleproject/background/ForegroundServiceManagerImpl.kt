package com.untitledkingdom.mybleproject.background

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import com.untitledkingdom.mybleproject.features.main.MainFragment
import com.untitledkingdom.mybleproject.features.welcome.WelcomeFragment
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Inject
import javax.inject.Singleton

@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@FlowPreview
@Singleton
class ForegroundServiceManagerImpl @Inject constructor(
    @ApplicationContext val context: Context
) : ForegroundServiceManager {
    override fun startService(fromClass: Class<*>) =
        when (fromClass) {
            MainFragment::class.java -> context.startForegroundService(
                Intent(context, ForegroundService::class.java).apply {
                    action = INTENT_ACTION_START_FROM_MAIN_SCREEN
                }
            )
            WelcomeFragment::class.java -> context.startForegroundService(
                Intent(context, ForegroundService::class.java).apply {
                    action = INTENT_ACTION_START_FROM_WELCOME_SCREEN
                }
            )
            else -> context.startForegroundService(
                Intent(context, ForegroundService::class.java)
            )
        }

    override fun stopService(): ComponentName? {
        val intent = Intent(context, ForegroundService::class.java)
        intent.action = INTENT_ACTION_STOP_SERVICE
        return context.startService(intent)
    }
}
