package com.untitledkingdom.mybleproject.bluetooth

import com.juul.kable.Advertisement
import com.juul.kable.State
import com.untitledkingdom.mybleproject.bluetooth.data.ReadingsData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow
import java.time.LocalDate

interface CustomBluetoothManager {
    val canUseBluetooth: Boolean
    val peripheralExists: Boolean
    val deviceAddress: String?
    val stateFlow: StateFlow<State>?
    val isConnected: Boolean

    @FlowPreview
    val flowReadings: Flow<ReadingsData>?
    val isConnecting: Boolean
    val isBluetoothEnabled: Boolean
    val hasBluetoothPermission: Boolean
    val getDeviceNameOrAddress: String?

    @FlowPreview
    val flowBatteryLevel: Flow<Short>?

    fun turnOnBluetooth()
    fun turnOffBluetooth()
    suspend fun connect(
        scope: CoroutineScope,
        advertisement: Advertisement,
        callbackConnected: () -> Unit = {},
        callbackError: (Exception, String) -> Unit = { _, _ -> }
    )

    suspend fun connect(
        scope: CoroutineScope,
        identifier: String,
        callbackConnected: () -> Unit = {},
        callbackError: (Exception, String) -> Unit = { _, _ -> }
    )

    suspend fun disconnect(): Unit?
    suspend fun getRssi(): Int?
    fun getDeviceName(): String?
    suspend fun writeReadingsCurrentTime()
    suspend fun reconnect(
        callbackError: (Exception, String) -> Unit = { _, _ -> }
    ): Unit?

    suspend fun getReadingsTime(): LocalDate?
    suspend fun readBattery(): Short?
}
