package com.untitledkingdom.mybleproject.bluetooth

import ReadingsOuterClass.Readings
import android.Manifest
import android.annotation.SuppressLint
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import com.juul.kable.Advertisement
import com.juul.kable.ConnectionLostException
import com.juul.kable.ConnectionRejectedException
import com.juul.kable.NotReadyException
import com.juul.kable.Peripheral
import com.juul.kable.State
import com.juul.kable.WriteType
import com.juul.kable.characteristicOf
import com.juul.kable.identifier
import com.juul.kable.logs.Logging
import com.juul.kable.peripheral
import com.untitledkingdom.mybleproject.R
import com.untitledkingdom.mybleproject.bluetooth.data.ReadingsData
import com.untitledkingdom.mybleproject.utils.date.TimeManagerImpl
import com.untitledkingdom.mybleproject.utils.getDeviceName
import com.untitledkingdom.mybleproject.utils.toByteArray
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.flatMapConcat
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withTimeoutOrNull
import timber.log.Timber
import java.time.LocalDate
import javax.inject.Inject
import javax.inject.Singleton

private const val TIMEOUT_DISCONNECT_MILLIS = 5_000L
private const val INDEX_YEAR_BYTE_1 = 2
private const val INDEX_YEAR_BYTE_2 = 3
private const val LENGTH_BYTES_YEAR = 2
private const val INDEX_DAY = 0
private const val INDEX_MONTH = 1

@Singleton
class CustomBluetoothManagerImpl @Inject constructor(
    @ApplicationContext private val applicationContext: Context,
    private val bluetoothManager: BluetoothManager,
) : CustomBluetoothManager {
    private val timeManager: TimeManagerImpl = TimeManagerImpl()
    private val bluetoothAdapter: BluetoothAdapter = bluetoothManager.adapter
    private var peripheral: Peripheral? = null
    override val peripheralExists: Boolean
        get() = peripheral != null

    override val getDeviceNameOrAddress
        get() = getDeviceName() ?: deviceAddress
    private val peripheralScope = CoroutineScope(Dispatchers.IO)

    @SuppressLint("MissingPermission")
    override fun getDeviceName() = peripheral?.getDeviceName(bluetoothAdapter = bluetoothAdapter)
    override val deviceAddress: String?
        get() = peripheral?.identifier

    override suspend fun getRssi() = try {
        peripheral?.rssi()
    } catch (e: NotReadyException) {
        Timber.e("peripheral.rssi was not ready e = $e")
        null
    }

    override val canUseBluetooth: Boolean
        get() = hasBluetoothPermission && isBluetoothEnabled

    @SuppressLint("MissingPermission")
    override fun turnOnBluetooth() {
        if (hasBluetoothPermission) {
            bluetoothAdapter.enable()
            Timber.d("Bluetooth enabled")
        } else
            Timber.d("Bluetooth not enabled: no permission")
    }

    @SuppressLint("MissingPermission")
    override fun turnOffBluetooth() {
        if (hasBluetoothPermission) {
            bluetoothAdapter.disable()
            Timber.d("Bluetooth disabled")
        } else
            Timber.d("Bluetooth not disabled: no permission")
    }

    override val hasBluetoothPermission
        get() =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S)
                hasBluetoothScanPermission() && hasBluetoothConnectPermission()
            else
                true

    @RequiresApi(Build.VERSION_CODES.S)
    private fun hasBluetoothConnectPermission() = ActivityCompat.checkSelfPermission(
        applicationContext,
        Manifest.permission.BLUETOOTH_CONNECT
    ) == PackageManager.PERMISSION_GRANTED

    @RequiresApi(Build.VERSION_CODES.S)
    private fun hasBluetoothScanPermission() = ActivityCompat.checkSelfPermission(
        applicationContext,
        Manifest.permission.BLUETOOTH_SCAN
    ) == PackageManager.PERMISSION_GRANTED

    override val isBluetoothEnabled: Boolean
        get() = bluetoothAdapter.isEnabled

    override suspend fun connect(
        scope: CoroutineScope,
        advertisement: Advertisement,
        callbackConnected: () -> Unit,
        callbackError: (
            exception: Exception,
            errorMessage: String
        ) -> Unit
    ) {
        peripheralScope.launch(Dispatchers.IO) {
            val peripheral = peripheral(advertisement = advertisement) {
                logging {
                    data = Logging.DataProcessor { bytes ->
                        bytes.joinToString { byte -> byte.toString() }
                    }
                    level = Logging.Level.Data
                }
                observationExceptionHandler { cause ->
                    Timber.d("Observation failure suppressed: $cause")
                }
                onServicesDiscovered {
                    Timber.d("onServicesDiscovered")
                }
            }
            connect(
                peripheral = peripheral,
                callbackConnected = callbackConnected,
                callbackError = callbackError
            )
        }
    }

    override suspend fun connect(
        scope: CoroutineScope,
        identifier: String,
        callbackConnected: () -> Unit,
        callbackError: (
            exception: Exception,
            errorMessage: String
        ) -> Unit
    ) {
        peripheralScope.launch(Dispatchers.IO) {
            val peripheral = peripheral(identifier = identifier) {
                logging {
                    data = Logging.DataProcessor { bytes ->
                        bytes.joinToString { byte -> byte.toString() }
                    }
                    level = Logging.Level.Data
                }
                observationExceptionHandler { cause ->
                    Timber.d("Observation failure suppressed: $cause")
                }
                onServicesDiscovered {
                    Timber.d("onServicesDiscovered")
                }
            }
            connect(
                peripheral = peripheral,
                callbackError = callbackError,
                callbackConnected = callbackConnected
            )
            Timber.d("peripheral connect return")
        }
    }

    private suspend fun connect(
        peripheral: Peripheral,
        callbackConnected: () -> Unit = {},
        callbackError: (exception: Exception, errorMessage: String) -> Unit
    ) {
        try {
            Timber.d("peripheral connect peripheral $peripheral")
            Timber.d("peripheral connect before .connect()")
            peripheral.connect()
            this.peripheral = peripheral
            callbackConnected()
            Timber.d("peripheral connect after .connect()")
        } catch (exception: Exception) {
            Timber.e("peripheral connect exception $exception")
            when (exception) {
                is ConnectionRejectedException -> callbackError(
                    exception,
                    applicationContext.getString(R.string.bluetooth_manager_connection_rejected)
                )
                is CancellationException -> callbackError(
                    exception,
                    applicationContext.getString(R.string.bluetooth_manager_connection_cancelled)
                )
                is ConnectionLostException ->
                    callbackError(
                        exception,
                        applicationContext.getString(R.string.bluetooth_manager_connection_lost)
                    )
                else -> {
                    Timber.e(exception)
                    callbackError(
                        exception,
                        applicationContext.getString(R.string.bluetooth_manager_unknown_connection_error)
                    )
                }
            }
        }
    }

    override suspend fun disconnect(): Unit? {
        val result = withTimeoutOrNull(TIMEOUT_DISCONNECT_MILLIS) {
            peripheral?.let { peripheral ->
                val statePeripheral = peripheral.state
                Timber.d("peripheral state before disconnect ${statePeripheral.value}")
                Timber.d("peripheral disconnecting...")
                try {
                    this@CustomBluetoothManagerImpl.peripheral = null
                    peripheral.disconnect()
                } catch (e: Exception) {
                    Timber.d("peripheral disconnect error $e")
                    Timber.d("peripheral disconnect error state = ${statePeripheral.value}")
                    return@let null
                }
                Timber.d("peripheral state after disconnect ${statePeripheral.value}")
            }
        }
        return result
    }

    @FlowPreview
    override val flowReadings: Flow<ReadingsData>?
        get() = peripheral?.let { peripheral ->
            val characteristic = getReadingsCharacteristic()
            return@let peripheral.observe(characteristic)
                .flatMapConcat { byteArray ->
                    flow {
                        try {
                            val readings = Readings.parseFrom(byteArray)
                            val readingsData = ReadingsData(
                                temperature = readings.temperature,
                                humidity = readings.hummidity
                            )
                            emit(readingsData)
                        } catch (e: Exception) {
                            Timber.e(e)
                        }
                    }
                }
        }

    @FlowPreview
    override val flowBatteryLevel: Flow<Short>?
        get() =
            peripheral?.let { peripheral ->
                val characteristic = getBatteryCharacteristic()
                peripheral.observe(characteristic)
                    .flatMapConcat { byteArray ->
                        flow {
                            try {
                                emit(getBatteryLevel(byteArray.first()))
                            } catch (e: Exception) {
                                Timber.e(e)
                            }
                        }
                    }
            }

    private fun getBatteryLevel(byte: Byte) = byte.toUShort().toShort()

    override suspend fun getReadingsTime() =
        peripheral?.let { peripheral ->
            val characteristic = getTimeCharacteristic()
            convertByteArrayToTime(peripheral.read(characteristic))
        }

    override suspend fun readBattery(): Short? =
        try {
            peripheral?.let { peripheral ->
                val batteryLevel =
                    getBatteryLevel(peripheral.read(getBatteryCharacteristic()).first())
                Timber.d("Read battery level = $batteryLevel")
                batteryLevel
            }
        } catch (e: Exception) {
            Timber.e("Failed to read battery e=$e")
            null
        }

    private fun getReadingsCharacteristic() = characteristicOf(
        ReadingConstants.UUID_SERVICE_READINGS,
        ReadingConstants.UUID_CHARACTERISTIC_READINGS
    )

    private fun getBatteryCharacteristic() = characteristicOf(
        ReadingConstants.UUID_SERVICE_BATTERY,
        ReadingConstants.UUID_CHARACTERISTIC_BATTERY
    )

    override val stateFlow: StateFlow<State>?
        get() = peripheral?.state

    private fun convertByteArrayToTime(byteArray: ByteArray) =
        try {
            val day = getDay(byteArray[INDEX_DAY])
            val month = getMonth(byteArray[INDEX_MONTH])
            val year = getYear(byteArray[INDEX_YEAR_BYTE_1], byteArray[INDEX_YEAR_BYTE_2])
            LocalDate.of(year, month, day)
        } catch (e: Exception) {
            Timber.e(e)
            null
        }

    override suspend fun writeReadingsCurrentTime() {
        try {
            peripheral?.write(
                characteristic = getTimeCharacteristic(),
                data = getCurrentTimeBytes(),
                writeType = WriteType.WithResponse
            )
        } catch (e: Exception) {
            Timber.e("Write exception $e")
        }
    }

    private fun getCurrentTimeBytes(): ByteArray {
        val localDateTime = timeManager.provideCurrentLocalDateTime()
        val day = localDateTime.dayOfMonth.toByte()
        val month = localDateTime.monthValue.toByte()
        val year = localDateTime.year.toByteArray(size = LENGTH_BYTES_YEAR)
        return byteArrayOf(day, month, year[LENGTH_BYTES_YEAR - 2], year[LENGTH_BYTES_YEAR - 1])
    }

    private fun getYear(firstByte: Byte, secondByte: Byte) =
        (firstByte.toUByte().toUInt() or (secondByte.toUByte().toUInt() shl Byte.SIZE_BITS)).toUShort().toInt()

    private fun getMonth(byte: Byte) = byte.toUShort().toInt()

    private fun getDay(byte: Byte) = byte.toUShort().toInt()

    override val isConnected: Boolean
        get() = stateFlow != null && stateFlow!!.value == State.Connected

    override val isConnecting: Boolean
        get() = peripheral != null && peripheral!!.state.value is State.Connecting

    override suspend fun reconnect(callbackError: (Exception, String) -> Unit) =
        peripheral?.let {
            connect(
                peripheral = it,
                callbackError = callbackError
            )
        }

    private fun getTimeCharacteristic() =
        characteristicOf(
            ReadingConstants.UUID_SERVICE_READINGS_TIME,
            ReadingConstants.UUID_CHARACTERISTIC_READINGS_TIME
        )
}
