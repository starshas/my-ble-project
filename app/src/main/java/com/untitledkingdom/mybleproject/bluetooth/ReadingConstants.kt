package com.untitledkingdom.mybleproject.bluetooth

object ReadingConstants {
    const val UUID_SERVICE_READINGS_TIME = "fd8136b0-f18f-4f36-ad03-c73311525a80"
    const val UUID_CHARACTERISTIC_READINGS_TIME = "fd8136b1-f18f-4f36-ad03-c73311525a80"
    const val UUID_SERVICE_READINGS = "fd8136c0-f18f-4f36-ad03-c73311525a80"
    const val UUID_CHARACTERISTIC_READINGS = "fd8136c1-f18f-4f36-ad03-c73311525a80"
    const val UUID_SERVICE_BATTERY = "fd8136d0-f18f-4f36-ad03-c73311525a80"
    const val UUID_CHARACTERISTIC_BATTERY = "fd8136d1-f18f-4f36-ad03-c73311525a80"
}
