package com.untitledkingdom.mybleproject.bluetooth.data

data class PeripheralInfo(
    val deviceName: String?,
    val address: String,
    val signalStrength: Int,
    val readingAt: String?,
    val temperature: Float?,
    val humidity: Int?
)
