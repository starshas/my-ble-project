package com.untitledkingdom.mybleproject.bluetooth.data

data class ReadingsData(
    val temperature: Float,
    val humidity: Int
)
