package com.untitledkingdom.mybleproject.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.untitledkingdom.mybleproject.database.data.ReadingsRecord

@Database(
    entities = [ReadingsRecord::class],
    version = DatabaseConstants.DATABASE_VERSION,
    exportSchema = false
)

@TypeConverters(CustomTypeConverters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getMainDao(): MainDao
}
