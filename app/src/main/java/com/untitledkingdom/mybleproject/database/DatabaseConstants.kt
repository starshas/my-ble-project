package com.untitledkingdom.mybleproject.database

object DatabaseConstants {
    const val DATABASE_NAME = "database"
    const val TABLE_READINGS = "readings"
    const val DATABASE_VERSION = 1

    object TableDayReadings {
        const val COLUMN_MIN_TEMPERATURE = "min_temperature"
        const val COLUMN_AVERAGE_TEMPERATURE = "average_temperature"
        const val COLUMN_MAX_TEMPERATURE = "max_temperature"
        const val COLUMN_MIN_HUMIDITY = "min_humidity"
        const val COLUMN_AVERAGE_HUMIDITY = "average_humidity"
        const val COLUMN_MAX_HUMIDITY = "max_humidity"
        const val COLUMN_DATE_AND_TIME = "date_and_time"
    }

    object TableReadings {
        const val COLUMN_DATE_AND_TIME = "date_and_time"
        const val COLUMN_TEMPERATURE = "temperature"
        const val COLUMN_HUMIDITY = "humidity"
        const val COLUMN_IS_SYNCED = "is_synced"
    }
}
