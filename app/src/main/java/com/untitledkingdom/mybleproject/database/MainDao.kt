package com.untitledkingdom.mybleproject.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.untitledkingdom.mybleproject.api.ApiConstants
import com.untitledkingdom.mybleproject.database.DatabaseConstants.TABLE_READINGS
import com.untitledkingdom.mybleproject.database.DatabaseConstants.TableDayReadings
import com.untitledkingdom.mybleproject.database.DatabaseConstants.TableReadings
import com.untitledkingdom.mybleproject.database.data.DayReadingsRecord
import com.untitledkingdom.mybleproject.database.data.ReadingsRecord
import java.time.OffsetDateTime

@Dao
interface MainDao {
    @Query(
        "SELECT * " +
            "FROM $TABLE_READINGS " +
            "WHERE date(${TableReadings.COLUMN_DATE_AND_TIME}) = date(:dayDate) " +
            "ORDER BY datetime(${TableReadings.COLUMN_DATE_AND_TIME}) DESC"
    )
    suspend fun getAllRecordsForTheDay(dayDate: OffsetDateTime): List<ReadingsRecord>

    @Query(
        "SELECT " +
            "${TableReadings.COLUMN_DATE_AND_TIME}, " +
            "min(${TableReadings.COLUMN_TEMPERATURE}) AS ${TableDayReadings.COLUMN_MIN_TEMPERATURE}," +
            "avg(${TableReadings.COLUMN_TEMPERATURE}) AS ${TableDayReadings.COLUMN_AVERAGE_TEMPERATURE}," +
            "max(${TableReadings.COLUMN_TEMPERATURE}) AS ${TableDayReadings.COLUMN_MAX_TEMPERATURE}," +
            "min(${TableReadings.COLUMN_HUMIDITY}) AS ${TableDayReadings.COLUMN_MIN_HUMIDITY}," +
            "avg(${TableReadings.COLUMN_HUMIDITY}) AS ${TableDayReadings.COLUMN_AVERAGE_HUMIDITY}," +
            "max(${TableReadings.COLUMN_HUMIDITY}) AS ${TableDayReadings.COLUMN_MAX_HUMIDITY} " +
            "FROM $TABLE_READINGS " +
            "GROUP BY date(${TableReadings.COLUMN_DATE_AND_TIME}) " +
            "ORDER BY datetime(${TableReadings.COLUMN_DATE_AND_TIME}) DESC"
    )
    suspend fun getAllDaysWithInfo(): List<DayReadingsRecord>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addRecord(readingsRecord: ReadingsRecord)

    @Query(
        "SELECT * " +
            "FROM $TABLE_READINGS " +
            "WHERE ${TableReadings.COLUMN_IS_SYNCED} = 0 " +
            "ORDER BY datetime(${TableReadings.COLUMN_DATE_AND_TIME}) " +
            "ASC LIMIT ${ApiConstants.NUMBER_OF_RECORDS_IN_SYNC_REQUEST}"
    )
    suspend fun getPortionOfFirstUnsyncedRecords(): List<ReadingsRecord>
}
