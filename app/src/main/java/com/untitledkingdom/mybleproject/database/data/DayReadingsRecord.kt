package com.untitledkingdom.mybleproject.database.data

import androidx.room.ColumnInfo
import com.untitledkingdom.mybleproject.database.DatabaseConstants.TableDayReadings
import java.time.OffsetDateTime

data class DayReadingsRecord(
    @ColumnInfo(name = TableDayReadings.COLUMN_MIN_TEMPERATURE)
    val minTemperature: Float,
    @ColumnInfo(name = TableDayReadings.COLUMN_AVERAGE_TEMPERATURE)
    val averageTemperature: Float,
    @ColumnInfo(name = TableDayReadings.COLUMN_MAX_TEMPERATURE)
    val maxTemperature: Float,
    @ColumnInfo(name = TableDayReadings.COLUMN_MIN_HUMIDITY)
    val minHumidity: Int,
    @ColumnInfo(name = TableDayReadings.COLUMN_AVERAGE_HUMIDITY)
    val averageHumidity: Int,
    @ColumnInfo(name = TableDayReadings.COLUMN_MAX_HUMIDITY)
    val maxHumidity: Int,
    @ColumnInfo(name = TableDayReadings.COLUMN_DATE_AND_TIME)
    val dateAndTime: OffsetDateTime
)
