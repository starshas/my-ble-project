package com.untitledkingdom.mybleproject.database.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.untitledkingdom.mybleproject.database.DatabaseConstants
import com.untitledkingdom.mybleproject.database.DatabaseConstants.TableReadings
import java.time.OffsetDateTime

@Entity(tableName = DatabaseConstants.TABLE_READINGS)
data class ReadingsRecord(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val temperature: Float,
    val humidity: Int,
    @ColumnInfo(name = TableReadings.COLUMN_DATE_AND_TIME)
    val dateAndTime: OffsetDateTime,
    @ColumnInfo(name = TableReadings.COLUMN_IS_SYNCED)
    val isSynced: Boolean = false
)
