package com.untitledkingdom.mybleproject.datastore

interface DataStorage {
    suspend fun saveToStorage(key: String, value: String)
    suspend fun getFromStorage(key: String): String
    suspend fun getBooleanFromStorage(key: String): Boolean
    suspend fun saveBooleanToStorage(key: String, value: Boolean)
}
