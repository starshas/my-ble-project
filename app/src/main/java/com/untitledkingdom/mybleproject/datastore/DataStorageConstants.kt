package com.untitledkingdom.mybleproject.datastore

object DataStorageConstants {
    const val DATA_STORE_NAME = "DATA_STORE"
    const val KEY_NAME_SAVED_DEVICE_IDENTIFIER = "KEY_NAME_SAVED_DEVICE_IDENTIFIER"
}

sealed interface UserAuth {
    object NotLoggedIn : UserAuth
    data class LoggedIn(val refreshToken: String,) : UserAuth
}
