package com.untitledkingdom.mybleproject.datastore

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import timber.log.Timber
import javax.inject.Inject

class DataStorageImpl @Inject constructor(
    @ApplicationContext val context: Context,
    private val dataStore: DataStore<Preferences>,
    private val security: SecureData
) : DataStorage {

    override suspend fun saveToStorage(key: String, value: String) {
        val encryptedValue = security.encrypt(value)
        dataStore.edit { dataStore ->
            dataStore[stringPreferencesKey(key)] = encryptedValue ?: ""
        }
        Timber.d(
            "Saving to storage with - key $key and value $value; " +
                "Value saved is $encryptedValue"
        )
    }

    override suspend fun getFromStorage(key: String): String {
        val valueFromStorage = dataStore.data.map { dataStore ->
            dataStore[stringPreferencesKey(key)] ?: ""
        }
        val decryptedValue = security.decrypt(valueFromStorage.first())
        Timber.d(
            "Getting from storage with - key $key, " +
                "value from storage was ${valueFromStorage.first()}" +
                "decrypted value is $decryptedValue"
        )
        return decryptedValue ?: ""
    }

    override suspend fun getBooleanFromStorage(key: String): Boolean {
        return getFromStorage(key).toBooleanOrFalse()
    }

    override suspend fun saveBooleanToStorage(key: String, value: Boolean) {
        saveToStorage(key, value.toString())
    }
}

private fun String.toBooleanOrFalse() = when (this) {
    "true" -> true
    else -> false
}
