package com.untitledkingdom.mybleproject.features.daydetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.findNavController
import com.tomcz.ellipse.common.onProcessor
import com.untitledkingdom.mybleproject.features.daydetails.state.DayDetailsEffect
import com.untitledkingdom.mybleproject.features.daydetails.state.DayDetailsEvent
import com.untitledkingdom.mybleproject.utils.OnBackPressed
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.FlowPreview
import java.time.OffsetDateTime

@FlowPreview
@AndroidEntryPoint
class DayDetailsFragment : Fragment(), OnBackPressed {
    private val viewModel: DayDetailsViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        onProcessor(
            lifecycleState = Lifecycle.State.RESUMED,
            processor = { viewModel.processor },
            onEffect = ::trigger
        )
        val stringBundleDate = arguments?.getString(DayDetailsConstants.BUNDLE_DATE)
        val offsetDateTime = OffsetDateTime.parse(stringBundleDate)

        viewModel.processor.sendEvent(DayDetailsEvent.SetListReadings(offsetDateTime))
        return ComposeView(
            requireContext()
        ).apply {
            setContent {
                DayDetailsScreenComposable(viewModel.processor)
            }
        }
    }

    private fun trigger(
        effect: DayDetailsEffect
    ) {
        when (effect) {
            DayDetailsEffect.GoBack -> findNavController().popBackStack()
        }
    }

    override fun onBackPressed() = true
}
