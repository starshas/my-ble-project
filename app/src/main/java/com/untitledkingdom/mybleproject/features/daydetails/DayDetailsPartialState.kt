package com.untitledkingdom.mybleproject.features.daydetails

import com.tomcz.ellipse.PartialState
import com.untitledkingdom.mybleproject.database.data.ReadingsRecord
import com.untitledkingdom.mybleproject.features.daydetails.state.DayDetailsState

sealed interface DayDetailsPartialState : PartialState<DayDetailsState> {
    data class SetReadingsListData(val listReadings: List<ReadingsRecord>) : DayDetailsPartialState {
        override fun reduce(oldState: DayDetailsState): DayDetailsState =
            oldState.copy(listReadings = listReadings)
    }
}
