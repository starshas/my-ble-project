package com.untitledkingdom.mybleproject.features.daydetails

import com.untitledkingdom.mybleproject.database.data.ReadingsRecord
import java.time.OffsetDateTime

interface DayDetailsRepository {
    suspend fun getReadingsForDay(dayDate: OffsetDateTime): List<ReadingsRecord>
}
