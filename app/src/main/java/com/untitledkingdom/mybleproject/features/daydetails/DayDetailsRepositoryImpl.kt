package com.untitledkingdom.mybleproject.features.daydetails

import com.untitledkingdom.mybleproject.database.AppDatabase
import timber.log.Timber
import java.time.OffsetDateTime
import javax.inject.Inject

class DayDetailsRepositoryImpl @Inject constructor(
    private val database: AppDatabase
) : DayDetailsRepository {
    override suspend fun getReadingsForDay(dayDate: OffsetDateTime) =
        try {
            database.getMainDao().getAllRecordsForTheDay(dayDate)
        } catch (e: Exception) {
            Timber.e(e)
            listOf()
        }
}
