package com.untitledkingdom.mybleproject.features.daydetails

import android.content.Context
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Divider
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.madrapps.plot.line.DataPoint
import com.madrapps.plot.line.LineGraph
import com.madrapps.plot.line.LinePlot
import com.tomcz.ellipse.common.collectAsState
import com.tomcz.ellipse.common.previewProcessor
import com.untitledkingdom.mybleproject.R
import com.untitledkingdom.mybleproject.features.daydetails.state.DayDetailsState
import com.untitledkingdom.mybleproject.ui.spacers.SpacerHeight
import com.untitledkingdom.mybleproject.ui.spacers.SpacerWidth
import com.untitledkingdom.mybleproject.ui.theme.Colors
import com.untitledkingdom.mybleproject.ui.theme.fontSize24
import com.untitledkingdom.mybleproject.ui.theme.padding8
import com.untitledkingdom.mybleproject.utils.date.DateFormatter.formatToDateAndTime

@Composable
fun DayDetailsScreenComposable(processor: DayDetailsProcessor) {
    val context = LocalContext.current

    Column {
        Title(context)
        Plot(processor = processor)
        SpacerHeight(height = padding8)
        PlotInfo(processor = processor)
        SpacerHeight(height = padding8)
        ListRecords(processor = processor)
    }
}

@Composable
private fun Title(context: Context) =
    Text(
        modifier = Modifier.padding(
            start = padding8,
            top = padding8,
            bottom = padding8
        ),
        text = context.getString(R.string.day_details_title),
        fontSize = fontSize24,
        fontWeight = FontWeight.Bold,
    )

@Composable
private fun PlotInfo(processor: DayDetailsProcessor) = Row {
    val listReadings by processor.collectAsState { it.listReadings }
    val context = LocalContext.current
    val stringOrange = context.getString(R.string.day_details_orange)
    val stringBlue = context.getString(R.string.day_details_blue)
    val stringIsTemperature = context.getString(R.string.day_details_is_temperature)
    val stringIsHumidity = context.getString(R.string.day_details_is_humidity)

    if (listReadings.isNotEmpty()) {
        SpacerWidth(width = padding8)
        Text(
            text = buildAnnotatedString {
                withStyle(
                    style = SpanStyle(
                        fontWeight = FontWeight.Bold,
                        color = Colors.Orange
                    )
                ) {
                    append(stringOrange)
                }
                append(" $stringIsTemperature ")
                withStyle(
                    style = SpanStyle(
                        fontWeight = FontWeight.Bold,
                        color = Colors.LightBlue
                    )
                ) {
                    append(stringBlue)
                }
                append(" $stringIsHumidity")
            }
        )
    }
}

@Composable
fun Plot(processor: DayDetailsProcessor) {
    val listReadings by processor.collectAsState { it.listReadings }

    val linesTemperature: List<DataPoint> = MutableList(listReadings.size) { index ->
        DataPoint(index.toFloat(), listReadings[index].temperature)
    }
    val linesHumidity: List<DataPoint> = MutableList(listReadings.size) { index ->
        DataPoint(index.toFloat(), listReadings[index].humidity.toFloat())
    }
    if (listReadings.isNotEmpty()) {
        LineGraph(
            plot = LinePlot(
                listOf(
                    LinePlot.Line(
                        linesHumidity,
                        LinePlot.Connection(color = Colors.LightBlue),
                        LinePlot.Intersection(color = Colors.LightBlue),
                        LinePlot.Highlight(color = Colors.LightBlue),
                    ),
                    LinePlot.Line(
                        linesTemperature,
                        LinePlot.Connection(color = Colors.Orange),
                        LinePlot.Intersection(color = Colors.Orange),
                        LinePlot.Highlight(color = Colors.Orange),
                    )
                ),
                grid = LinePlot.Grid(Color.Green, steps = 4),
            ),
            modifier = Modifier
                .fillMaxWidth()
                .padding(end = 16.dp)
                .height(200.dp)
        )
    }
}

@Composable
private fun ListRecords(processor: DayDetailsProcessor) {
    val items by processor.collectAsState { it.listReadings }

    LazyColumn(
        modifier = Modifier.padding(start = padding8),
        content = {
            items(items.size) { index ->
                val item = items[index]

                ItemReading(
                    time = item.dateAndTime.formatToDateAndTime(),
                    temperature = item.temperature,
                    humidity = item.humidity,
                    isSynced = item.isSynced
                )
            }
        }
    )
}

@Composable
fun ItemReading(
    time: String,
    temperature: Float,
    humidity: Int,
    isSynced: Boolean
) {
    val context = LocalContext.current
    val stringTemperature = context.getString(R.string.day_details_temperature_is)
    val stringHumidity = context.getString(R.string.day_details_humidity_is)
    val stringSynced = context.getString(R.string.day_details_synced_is)
    val synced =
        if (isSynced) context.getString(R.string.common_yes)
        else context.getString(R.string.common_no)

    Column {
        SpacerHeight(height = padding8)
        Text(text = time)
        Text(text = "$stringTemperature $temperature")
        Text(text = "$stringHumidity $humidity")
        Text(text = "$stringSynced $synced")
        SpacerHeight(height = padding8)
        Divider()
    }
}

@Preview(showBackground = true)
@Composable
private fun Preview() =
    DayDetailsScreenComposable(
        processor = previewProcessor(DayDetailsState())
    )
