package com.untitledkingdom.mybleproject.features.daydetails

import androidx.lifecycle.ViewModel
import com.tomcz.ellipse.Processor
import com.tomcz.ellipse.common.EllipseContext
import com.tomcz.ellipse.common.processor
import com.tomcz.ellipse.common.toNoAction
import com.untitledkingdom.mybleproject.features.daydetails.state.DayDetailsEffect
import com.untitledkingdom.mybleproject.features.daydetails.state.DayDetailsEvent
import com.untitledkingdom.mybleproject.features.daydetails.state.DayDetailsState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import timber.log.Timber
import java.time.OffsetDateTime
import javax.inject.Inject

typealias DayDetailsProcessor = Processor<DayDetailsEvent, DayDetailsState, DayDetailsEffect>

@HiltViewModel
class DayDetailsViewModel @Inject constructor(
    private val repository: DayDetailsRepository
) : ViewModel() {
    @FlowPreview
    val processor: DayDetailsProcessor = processor(
        initialState = DayDetailsState()
    ) { event ->
        when (event) {
            is DayDetailsEvent.SetListReadings -> handleSetListReadings(event.dayDate)
            is DayDetailsEvent.GoBack -> handleEventGoBack().toNoAction()
        }
    }

    private suspend fun handleSetListReadings(dayDate: OffsetDateTime): Flow<DayDetailsPartialState> {
        val listReadings = repository.getReadingsForDay(dayDate)
        Timber.d("listReadings = $listReadings")
        return flowOf(DayDetailsPartialState.SetReadingsListData(listReadings))
    }

    private fun EllipseContext<DayDetailsState, DayDetailsEffect>.handleEventGoBack() {
        effects.send(DayDetailsEffect.GoBack)
    }
}
