package com.untitledkingdom.mybleproject.features.daydetails.state

sealed interface DayDetailsEffect {
    object GoBack : DayDetailsEffect
}
