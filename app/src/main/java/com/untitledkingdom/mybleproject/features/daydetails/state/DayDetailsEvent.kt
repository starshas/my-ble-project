package com.untitledkingdom.mybleproject.features.daydetails.state

import java.time.OffsetDateTime

sealed interface DayDetailsEvent {
    data class SetListReadings(val dayDate: OffsetDateTime) : DayDetailsEvent
    object GoBack : DayDetailsEvent
}
