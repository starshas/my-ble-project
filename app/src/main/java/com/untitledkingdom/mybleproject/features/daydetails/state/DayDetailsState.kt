package com.untitledkingdom.mybleproject.features.daydetails.state

import com.untitledkingdom.mybleproject.database.data.ReadingsRecord

data class DayDetailsState(val listReadings: List<ReadingsRecord> = listOf())
