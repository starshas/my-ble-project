package com.untitledkingdom.mybleproject.features.history

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.findNavController
import com.tomcz.ellipse.common.onProcessor
import com.untitledkingdom.mybleproject.R
import com.untitledkingdom.mybleproject.features.daydetails.DayDetailsConstants
import com.untitledkingdom.mybleproject.features.main.statehistory.HistoryEffect
import com.untitledkingdom.mybleproject.utils.OnBackPressed
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.FlowPreview
import java.time.OffsetDateTime

@FlowPreview
@AndroidEntryPoint
class HistoryFragment : Fragment(), OnBackPressed {
    private val viewModel: HistoryViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        onProcessor(
            lifecycleState = Lifecycle.State.RESUMED,
            processor = { viewModel.processor },
            onEffect = ::trigger
        )
        return ComposeView(
            requireContext()
        ).apply {
            setContent {
                HistoryScreenComposable(viewModel.processor)
            }
        }
    }

    private fun trigger(
        effect: HistoryEffect
    ) {
        when (effect) {
            HistoryEffect.GoBack -> goBack()
            is HistoryEffect.GoToDayDetailsScreen -> goToDayDetailsScreen(effect.date)
        }
    }

    private fun goBack() = findNavController().popBackStack()

    private fun goToDayDetailsScreen(date: OffsetDateTime) {
        val bundle = Bundle().apply { putString(DayDetailsConstants.BUNDLE_DATE, date.toString()) }
        findNavController().navigate(R.id.action_historyFragment_to_dayDetails, bundle)
    }

    override fun onBackPressed() = true
}
