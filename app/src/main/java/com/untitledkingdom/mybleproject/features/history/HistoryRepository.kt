package com.untitledkingdom.mybleproject.features.history

import com.untitledkingdom.mybleproject.database.data.DayReadingsRecord

interface HistoryRepository {
    suspend fun getAllDaysWithInfo(): List<DayReadingsRecord>
}
