package com.untitledkingdom.mybleproject.features.history

import com.untitledkingdom.mybleproject.database.AppDatabase
import com.untitledkingdom.mybleproject.database.data.DayReadingsRecord
import timber.log.Timber
import javax.inject.Inject

class HistoryRepositoryImpl @Inject constructor(
    private val database: AppDatabase
) : HistoryRepository {
    override suspend fun getAllDaysWithInfo(): List<DayReadingsRecord> =
        try {
            database.getMainDao().getAllDaysWithInfo()
        } catch (e: Exception) {
            Timber.e(e)
            emptyList()
        }
}
