package com.untitledkingdom.mybleproject.features.history

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Divider
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import com.tomcz.ellipse.common.collectAsState
import com.tomcz.ellipse.common.previewProcessor
import com.untitledkingdom.mybleproject.R
import com.untitledkingdom.mybleproject.features.main.data.DayInfo
import com.untitledkingdom.mybleproject.features.main.statehistory.HistoryEvent
import com.untitledkingdom.mybleproject.features.main.statehistory.HistoryState
import com.untitledkingdom.mybleproject.ui.spacers.SpacerHeight
import com.untitledkingdom.mybleproject.ui.theme.fontSize24
import com.untitledkingdom.mybleproject.ui.theme.padding8
import com.untitledkingdom.mybleproject.utils.date.DateFormatter.formatToDate
import java.math.BigDecimal
import java.math.RoundingMode
import java.time.OffsetDateTime

@Composable
fun HistoryScreenComposable(processor: HistoryProcessor) {
    val context = LocalContext.current
    val staringTitle = context.getString(R.string.history_title)

    Column {
        Title(staringTitle)
        ListReadings(processor)
    }
}

@Composable
private fun Title(staringTitle: String) =
    Text(
        modifier = Modifier.padding(
            start = padding8,
            top = padding8,
            bottom = padding8
        ),
        text = staringTitle,
        fontSize = fontSize24,
        fontWeight = FontWeight.Bold,
    )

@Composable
private fun ListReadings(processor: HistoryProcessor) {
    val items by processor.collectAsState { it.listDaysWithInfo }
    LazyColumn(
        modifier = Modifier.padding(start = padding8),
        content = {
            items(items.size) { index ->
                val item = items[index]

                ItemDayInfo(
                    processor = processor,
                    dayInfo = DayInfo(
                        date = item.dateAndTime,
                        minTemperature = item.minTemperature.roundTo(2),
                        averageTemperature = item.averageTemperature.roundTo(2),
                        maxTemperature = item.maxTemperature.roundTo(2),
                        minHumidity = item.minHumidity,
                        averageHumidity = item.averageHumidity,
                        maxHumidity = item.maxHumidity
                    )
                )
            }
        }
    )
}

private fun Float.roundTo(decimals: Int): Float =
    BigDecimal(this.toDouble()).setScale(decimals, RoundingMode.FLOOR).toFloat()

@Composable
private fun ItemDayInfo(
    processor: HistoryProcessor,
    dayInfo: DayInfo
) = Column(
    modifier = Modifier.clickable {
        actionItemClicked(processor = processor, date = dayInfo.date)
    }
) {
    SpacerHeight(height = padding8)
    Text(text = dayInfo.date.formatToDate())
    SpacerHeight(height = padding8)

    val context = LocalContext.current
    val stringTemperatureIs = context.getString(R.string.history_temperature_is)
    val stringHumidityIs = context.getString(R.string.history_humidity_is)
    val min = context.getString(R.string.history_min_is)
    val avg = context.getString(R.string.history_avg_is)
    val max = context.getString(R.string.history_max_is)
    Text(
        text = "$stringTemperatureIs " +
                "$min ${dayInfo.minTemperature}, " +
                "$avg ${dayInfo.averageTemperature}, " +
                "$max ${dayInfo.maxTemperature}"
    )
    Text(
        text = "$stringHumidityIs " +
                "$min ${dayInfo.minHumidity}, " +
                "$avg ${dayInfo.averageHumidity}, " +
                "$max ${dayInfo.maxHumidity}"
    )
    SpacerHeight(height = padding8)
    Divider()
}

fun actionItemClicked(
    processor: HistoryProcessor,
    date: OffsetDateTime
) = processor.sendEvent(HistoryEvent.GoToDayDetailsScreen(date))

@Preview(showBackground = true)
@Composable
private fun Preview() =
    HistoryScreenComposable(
        processor = previewProcessor(HistoryState())
    )
