package com.untitledkingdom.mybleproject.features.history

import androidx.lifecycle.ViewModel
import com.tomcz.ellipse.Processor
import com.tomcz.ellipse.common.EllipseContext
import com.tomcz.ellipse.common.processor
import com.tomcz.ellipse.common.toNoAction
import com.untitledkingdom.mybleproject.features.main.statehistory.HistoryEffect
import com.untitledkingdom.mybleproject.features.main.statehistory.HistoryEvent
import com.untitledkingdom.mybleproject.features.main.statehistory.HistoryPartialState
import com.untitledkingdom.mybleproject.features.main.statehistory.HistoryState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

typealias HistoryProcessor = Processor<HistoryEvent, HistoryState, HistoryEffect>

@HiltViewModel
class HistoryViewModel @Inject constructor(
    private val repository: HistoryRepository
) : ViewModel() {
    @FlowPreview
    val processor: HistoryProcessor = processor(
        initialState = HistoryState(),
        prepare = { prepare() }
    ) { event ->
        when (event) {
            is HistoryEvent.GoBack -> handleEventGoBack().toNoAction()
            is HistoryEvent.GoToDayDetailsScreen -> effects.send(
                HistoryEffect.GoToDayDetailsScreen(event.date)
            ).toNoAction()
        }
    }

    private fun prepare() = flow {
        val listDaysWithInfo = repository.getAllDaysWithInfo()
        emit(HistoryPartialState.SetListDaysWithInfo(listDaysWithInfo = listDaysWithInfo))
    }

    private fun EllipseContext<HistoryState, HistoryEffect>.handleEventGoBack() =
        effects.send(HistoryEffect.GoBack)
}
