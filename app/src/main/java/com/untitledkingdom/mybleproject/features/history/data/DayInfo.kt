package com.untitledkingdom.mybleproject.features.history.data

data class DayInfo(
    val date: String,
    val minTemperature: Float,
    val averageTemperature: Float,
    val maxTemperature: Float,
    val minHumidity: Int,
    val averageHumidity: Int,
    val maxHumidity: Int
)
