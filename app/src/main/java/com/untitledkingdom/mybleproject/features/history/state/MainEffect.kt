package com.untitledkingdom.mybleproject.features.history.state

sealed interface MainEffect {
    data class ShowToast(
        val resId: Int,
        val parameterString: String? = null
    ) : MainEffect

    object GoBack : MainEffect
    object GoToHistoryScreen : MainEffect
}
