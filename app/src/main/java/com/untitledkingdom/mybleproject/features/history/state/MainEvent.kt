package com.untitledkingdom.mybleproject.features.history.state

sealed interface MainEvent {
    object GoBack : MainEvent
    object GoToHistoryScreen : MainEvent
}
