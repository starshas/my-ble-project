package com.untitledkingdom.mybleproject.features.history.state

import com.tomcz.ellipse.PartialState
import com.untitledkingdom.mybleproject.features.main.data.MainInfo

sealed interface MainPartialState : PartialState<MainState> {
    data class SetMainInfo(
        val mainInfo: MainInfo
    ) : MainPartialState {
        override fun reduce(oldState: MainState) =
            oldState.copy(
                deviceName = mainInfo.deviceName,
                deviceAddress = mainInfo.address,
                temperature = mainInfo.temperature,
                humidity = mainInfo.humidity,
                signalStrength = mainInfo.signalStrength
            )
    }

    data class SetReadingsTime(val time: String) : MainPartialState {
        override fun reduce(oldState: MainState) =
            oldState.copy(readingAt = time)
    }
}
