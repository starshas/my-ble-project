package com.untitledkingdom.mybleproject.features.history.state

data class MainState(
    val deviceName: String? = null,
    val deviceAddress: String? = null,
    val humidity: Int? = null,
    val temperature: Float? = null,
    val signalStrength: Int? = null,
    val readingAt: String? = null
)
