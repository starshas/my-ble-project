package com.untitledkingdom.mybleproject.features.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.findNavController
import com.tomcz.ellipse.common.onProcessor
import com.untitledkingdom.mybleproject.R
import com.untitledkingdom.mybleproject.background.ForegroundServiceManager
import com.untitledkingdom.mybleproject.features.daydetails.DayDetailsConstants
import com.untitledkingdom.mybleproject.features.main.state.MainEffect
import com.untitledkingdom.mybleproject.features.main.state.MainEvent
import com.untitledkingdom.mybleproject.utils.OnBackPressed
import com.untitledkingdom.mybleproject.utils.showToast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.FlowPreview
import timber.log.Timber
import java.time.OffsetDateTime
import javax.inject.Inject

@FlowPreview
@AndroidEntryPoint
class MainFragment : Fragment(), OnBackPressed {
    private val viewModel: MainViewModel by viewModels()

    @Inject
    lateinit var foregroundServiceManager: ForegroundServiceManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        onProcessor(
            lifecycleState = Lifecycle.State.RESUMED,
            processor = { viewModel.processor },
            onEffect = ::trigger
        )
        return ComposeView(
            requireContext()
        ).apply {
            setContent {
                MainScreenComposable(viewModel.processor)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        foregroundServiceManager.startService(this::class.java)
    }

    override fun onResume() {
        super.onResume()
        val backQueue = findNavController().backQueue
        Timber.d("backQueue ----------")
        for (item in backQueue)
            Timber.d("backQueue = ${item.destination.displayName}")
        Timber.d("Main onResume backstack")
    }

    private fun trigger(effect: MainEffect) {
        val context = requireActivity().applicationContext

        when (effect) {
            is MainEffect.ShowToast -> context.showToast(
                resId = effect.resId,
                parameterString = effect.parameterString
            )
            MainEffect.GoBack -> goBack()
            MainEffect.GoToHistoryScreen -> goToHistory()
            MainEffect.GoToSettingsScreen -> goToSettingsScreen()
            is MainEffect.GoToDayDetailsScreen -> goToDayDetailsScreen(effect.data)
        }
    }

    private fun goToDayDetailsScreen(date: OffsetDateTime) {
        val bundle = Bundle().apply { putString(DayDetailsConstants.BUNDLE_DATE, date.toString()) }
        findNavController().navigate(R.id.action_mainFragment_to_dayDetailsFragment, bundle)
    }

    private fun goBack() {
        foregroundServiceManager.stopService()
        findNavController().navigate(R.id.action_mainFragment_to_welcomeFragment)
    }

    private fun goToSettingsScreen() =
        findNavController().navigate(R.id.action_mainFragment_to_settingsFragment)

    private fun goToHistory() =
        findNavController().navigate(R.id.action_mainFragment_to_historyFragment)

    override fun onBackPressed(): Boolean {
        viewModel.processor.sendEvent(MainEvent.GoBack)
        return false
    }
}
