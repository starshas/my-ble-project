package com.untitledkingdom.mybleproject.features.main

import android.content.Context
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.tomcz.ellipse.common.collectAsState
import com.tomcz.ellipse.common.previewProcessor
import com.untitledkingdom.mybleproject.R
import com.untitledkingdom.mybleproject.features.main.state.MainEvent
import com.untitledkingdom.mybleproject.features.main.state.MainState
import com.untitledkingdom.mybleproject.ui.spacers.SpacerHeight
import com.untitledkingdom.mybleproject.ui.spacers.SpacerWidth
import com.untitledkingdom.mybleproject.ui.theme.padding8
import com.untitledkingdom.mybleproject.utils.toStringOrEmpty

@Composable
fun MainScreenComposable(processor: MainProcessor) {
    val deviceName by processor.collectAsState { it.deviceName }
    val deviceAddress by processor.collectAsState { it.deviceAddress }
    val temperature by processor.collectAsState { it.temperature }
    val humidity by processor.collectAsState { it.humidity }
    val signalStrength by processor.collectAsState { it.signalStrength }
    val readingAt by processor.collectAsState { it.readingAt }
    val batteryLevel by processor.collectAsState { it.batteryLevel }
    val context = LocalContext.current
    val stringTabHistory = context.getString(R.string.main_tab_history)
    val stringTabSettings = context.getString(R.string.main_tab_settings)
    val stringTabCurrentDay = context.getString(R.string.main_tab_current_day)

    Tabs(processor, stringTabHistory, stringTabSettings, stringTabCurrentDay)

    ReadInfo(
        context,
        deviceName,
        deviceAddress,
        signalStrength,
        readingAt,
        temperature,
        humidity,
        batteryLevel
    )
}

@Composable
private fun ReadInfo(
    context: Context,
    deviceName: String?,
    deviceAddress: String?,
    signalStrength: Int?,
    readingAt: String?,
    temperature: Float?,
    humidity: Int?,
    batteryLevel: Short?
) {
    val stringDeviceName = context.getString(R.string.main_device_name)
    val stringAddress = context.getString(R.string.main_address)
    val stringSignalStrength = context.getString(R.string.main_signal_strength)
    val stringReadingAt = context.getString(R.string.main_reading_at)
    val stringTemperature = context.getString(R.string.main_temperature)
    val stringHumidity = context.getString(R.string.main_humidity)
    val stringBatteryLevel = context.getString(R.string.main_battery_level)

    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        RowText(description = stringDeviceName, value = deviceName.toStringOrEmpty())
        RowText(description = stringAddress, value = deviceAddress.toStringOrEmpty())
        RowText(description = stringSignalStrength, value = signalStrength.toStringOrEmpty())
        RowText(description = stringReadingAt, value = readingAt.toStringOrEmpty())
        RowText(description = stringTemperature, value = temperature.toStringOrEmpty())
        RowText(description = stringHumidity, value = humidity.toStringOrEmpty())
        RowText(description = stringBatteryLevel, value = batteryLevel.toStringOrEmpty())
    }
}

@Composable
private fun Tabs(
    processor: MainProcessor,
    stringTabHistory: String,
    stringTabSettings: String,
    stringTabCurrentDay: String
) {
    Row(
        modifier = Modifier
            .padding(top = 16.dp),
        horizontalArrangement = Arrangement.Center
    ) {
        SpacerWidth(width = padding8)
        Button(
            modifier = Modifier.weight(1f),
            onClick = { actionHistoryClick(processor = processor) }
        ) {
            Text(text = stringTabHistory)
        }
        SpacerWidth(width = padding8)
        Button(
            modifier = Modifier.weight(1f),
            onClick = { actionGoToSettings(processor = processor) }
        ) {
            Text(text = stringTabSettings)
        }
        SpacerWidth(width = padding8)
        Button(
            modifier = Modifier.weight(1f),
            onClick = { goToCurrentDay(processor = processor) }
        ) {
            Text(text = stringTabCurrentDay)
        }
        SpacerWidth(width = padding8)
    }
}

fun goToCurrentDay(processor: MainProcessor) =
    processor.sendEvent(MainEvent.GoToCurrentDay)

fun actionGoToSettings(processor: MainProcessor) =
    processor.sendEvent(MainEvent.GoToSettingsScreen)

private fun actionHistoryClick(processor: MainProcessor) =
    processor.sendEvent(MainEvent.GoToHistoryScreen)

@Composable
private fun RowText(
    description: String,
    value: String
) = Column {
    Row(
        modifier = Modifier.width(250.dp),
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Text(
            text = description,
            textAlign = TextAlign.Start,
            fontWeight = FontWeight.Bold
        )
        Text(
            textAlign = TextAlign.End,
            text = value
        )
    }
    SpacerHeight(height = padding8)
}

@Preview(showBackground = true)
@Composable
private fun Preview() =
    MainScreenComposable(
        processor = previewProcessor(MainState())
    )
