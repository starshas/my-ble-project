package com.untitledkingdom.mybleproject.features.main

import androidx.lifecycle.ViewModel
import com.tomcz.ellipse.EffectsCollector
import com.tomcz.ellipse.PartialState
import com.tomcz.ellipse.Processor
import com.tomcz.ellipse.common.EllipseContext
import com.tomcz.ellipse.common.NoAction
import com.tomcz.ellipse.common.processor
import com.tomcz.ellipse.common.toNoAction
import com.untitledkingdom.mybleproject.R
import com.untitledkingdom.mybleproject.bluetooth.CustomBluetoothManager
import com.untitledkingdom.mybleproject.datastore.DataStorage
import com.untitledkingdom.mybleproject.datastore.DataStorageConstants
import com.untitledkingdom.mybleproject.features.main.data.MainInfo
import com.untitledkingdom.mybleproject.features.main.state.MainEffect
import com.untitledkingdom.mybleproject.features.main.state.MainEvent
import com.untitledkingdom.mybleproject.features.main.state.MainPartialState
import com.untitledkingdom.mybleproject.features.main.state.MainState
import com.untitledkingdom.mybleproject.utils.date.DateFormatter.basicFormat
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flatMapConcat
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.flow.onEach
import timber.log.Timber
import java.time.OffsetDateTime
import javax.inject.Inject

typealias MainProcessor = Processor<MainEvent, MainState, MainEffect>

@HiltViewModel
class MainViewModel @Inject constructor(
    private val dataStorage: DataStorage,
    private val customBluetoothManager: CustomBluetoothManager,
) : ViewModel() {
    @FlowPreview
    val processor: MainProcessor = processor(
        initialState = MainState(),
        prepare = {
            Timber.d("Main prepare()")
            prepare()
        }
    ) { event ->
        when (event) {
            is MainEvent.GoBack -> handleGoBack().toNoAction()
            MainEvent.GoToHistoryScreen -> effects.send(MainEffect.GoToHistoryScreen).toNoAction()
            MainEvent.GoToSettingsScreen -> effects.send(MainEffect.GoToSettingsScreen).toNoAction()
            is MainEvent.GoToCurrentDay -> effects.send(
                MainEffect.GoToDayDetailsScreen(OffsetDateTime.now())
            ).toNoAction()
        }
    }

    private suspend fun EllipseContext<MainState, MainEffect>.handleGoBack() {
        dataStorage.saveToStorage(
            key = DataStorageConstants.KEY_NAME_SAVED_DEVICE_IDENTIFIER,
            value = ""
        )
        disconnectFromDevice(effects)
        effects.send(MainEffect.GoBack)
    }

    @FlowPreview
    private fun EllipseContext<MainState, MainEffect>.prepare(): Flow<PartialState<MainState>> =
        merge(
            getInitialData(),
            getFlowStateFlow(),
            getFlowReadings(),
            getFlowBatteryLevel()
        )

    private fun getInitialData(): Flow<PartialState<MainState>> = flow {
        with(customBluetoothManager) {
            Timber.d(
                "deviceName = ${getDeviceName()}, " +
                    "deviceAddress = $deviceAddress, " +
                    "signalStrength = ${getRssi()}"
            )
            emit(
                MainPartialState.SetInitialData(
                    deviceName = getDeviceName(),
                    deviceAddress = deviceAddress,
                    signalStrength = getRssi(),
                )
            )
            emit(
                MainPartialState.SetBatteryLevel(
                    customBluetoothManager.readBattery()
                )
            )
        }
    }

    @FlowPreview
    private fun getFlowBatteryLevel() =
        customBluetoothManager.flowBatteryLevel
            ?.map {
                MainPartialState.SetBatteryLevel(it)
            } ?: toNoAction()

    private fun EllipseContext<MainState, MainEffect>.getFlowStateFlow(): Flow<PartialState<MainState>> =
        customBluetoothManager.stateFlow
            ?.onEach {
                Timber.d("Peripheral state in Main $it")
            }?.map {
                NoAction()
            } ?: toNoAction()

    @FlowPreview
    private fun getFlowReadings(): Flow<PartialState<MainState>> {
        Timber.d("getFlowReadings() ${customBluetoothManager.flowReadings}")
        return customBluetoothManager.flowReadings
            ?.flatMapConcat { readingsData ->
                Timber.d("added readingsRecord")
                flow {
                    emit(
                        MainPartialState.SetMainInfo(
                            MainInfo(
                                deviceName = customBluetoothManager.getDeviceName(),
                                address = customBluetoothManager.deviceAddress,
                                signalStrength = customBluetoothManager.getRssi(),
                                temperature = readingsData.temperature,
                                humidity = readingsData.humidity
                            )
                        )
                    )
                    customBluetoothManager.writeReadingsCurrentTime()
                    val readingsTime = customBluetoothManager.getReadingsTime()
                    readingsTime?.let {
                        emit(MainPartialState.SetReadingsTime(it.basicFormat()))
                    }
                }.catch { e -> Timber.e(e) }
            } ?: toNoAction()
    }

    private suspend fun disconnectFromDevice(effects: EffectsCollector<MainEffect>) {
        Timber.d("disconnectFromDevice start")
        val isDisconnectSuccessful = customBluetoothManager.disconnect() != null
        Timber.d("disconnectFromDevice isDisconnectSuccessful = $isDisconnectSuccessful")
        if (isDisconnectSuccessful)
            effects.send(MainEffect.ShowToast(resId = R.string.main_disconnect_successful))
        else
            effects.send(MainEffect.ShowToast(resId = R.string.main_disconnect_unsuccessful))
        Timber.d("disconnectFromDevice end")
    }
}
