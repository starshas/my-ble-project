package com.untitledkingdom.mybleproject.features.main.data

import java.time.OffsetDateTime

data class DayInfo(
    val date: OffsetDateTime,
    val minTemperature: Float,
    val averageTemperature: Float,
    val maxTemperature: Float,
    val minHumidity: Int,
    val averageHumidity: Int,
    val maxHumidity: Int
)
