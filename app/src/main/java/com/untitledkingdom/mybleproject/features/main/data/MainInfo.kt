package com.untitledkingdom.mybleproject.features.main.data

data class MainInfo(
    val deviceName: String?,
    val address: String?,
    val signalStrength: Int?,
    val temperature: Float?,
    val humidity: Int?
)
