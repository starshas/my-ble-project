package com.untitledkingdom.mybleproject.features.main.state

import java.time.OffsetDateTime

sealed interface MainEffect {
    data class ShowToast(
        val resId: Int,
        val parameterString: String? = null
    ) : MainEffect

    object GoBack : MainEffect
    object GoToHistoryScreen : MainEffect
    object GoToSettingsScreen : MainEffect
    data class GoToDayDetailsScreen(val data: OffsetDateTime) : MainEffect
}
