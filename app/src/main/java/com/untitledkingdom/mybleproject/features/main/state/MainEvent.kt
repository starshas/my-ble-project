package com.untitledkingdom.mybleproject.features.main.state

sealed interface MainEvent {
    object GoBack : MainEvent
    object GoToHistoryScreen : MainEvent
    object GoToSettingsScreen : MainEvent
    object GoToCurrentDay : MainEvent
}
