package com.untitledkingdom.mybleproject.features.main.state

import com.tomcz.ellipse.PartialState
import com.untitledkingdom.mybleproject.features.main.data.MainInfo

sealed interface MainPartialState : PartialState<MainState> {
    data class SetMainInfo(
        val mainInfo: MainInfo
    ) : MainPartialState {
        override fun reduce(oldState: MainState) =
            oldState.copy(
                deviceName = mainInfo.deviceName,
                deviceAddress = mainInfo.address,
                temperature = mainInfo.temperature,
                humidity = mainInfo.humidity,
                signalStrength = mainInfo.signalStrength
            )
    }

    data class SetInitialData(
        val deviceName: String?,
        val deviceAddress: String?,
        val signalStrength: Int?
    ) : MainPartialState {
        override fun reduce(oldState: MainState) =
            oldState.copy(
                deviceName = deviceName,
                deviceAddress = deviceAddress,
                signalStrength = signalStrength
            )
    }

    data class SetReadingsTime(val time: String?) : MainPartialState {
        override fun reduce(oldState: MainState) =
            oldState.copy(readingAt = time)
    }

    data class SetBatteryLevel(val batteryLevel: Short?) : MainPartialState {
        override fun reduce(oldState: MainState) =
            oldState.copy(batteryLevel = batteryLevel)
    }
}
