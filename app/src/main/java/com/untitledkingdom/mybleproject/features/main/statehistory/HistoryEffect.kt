package com.untitledkingdom.mybleproject.features.main.statehistory

import java.time.OffsetDateTime

sealed interface HistoryEffect {
    data class GoToDayDetailsScreen(val date: OffsetDateTime) : HistoryEffect
    object GoBack : HistoryEffect
}
