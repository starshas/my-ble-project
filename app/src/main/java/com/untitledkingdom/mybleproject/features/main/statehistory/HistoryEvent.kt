package com.untitledkingdom.mybleproject.features.main.statehistory

import java.time.OffsetDateTime

sealed interface HistoryEvent {
    object GoBack : HistoryEvent
    data class GoToDayDetailsScreen(val date: OffsetDateTime) : HistoryEvent
}
