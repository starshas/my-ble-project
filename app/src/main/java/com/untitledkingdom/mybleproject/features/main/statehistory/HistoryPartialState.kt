package com.untitledkingdom.mybleproject.features.main.statehistory

import com.tomcz.ellipse.PartialState
import com.untitledkingdom.mybleproject.database.data.DayReadingsRecord

sealed interface HistoryPartialState : PartialState<HistoryState> {
    data class SetListDaysWithInfo(
        val listDaysWithInfo: List<DayReadingsRecord>
    ) : HistoryPartialState {
        override fun reduce(oldState: HistoryState): HistoryState =
            oldState.copy(listDaysWithInfo = listDaysWithInfo)
    }
}
