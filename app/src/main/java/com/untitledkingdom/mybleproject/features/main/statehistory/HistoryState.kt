package com.untitledkingdom.mybleproject.features.main.statehistory

import com.untitledkingdom.mybleproject.database.data.DayReadingsRecord

data class HistoryState(
    val listDaysWithInfo: List<DayReadingsRecord> = listOf()
)
