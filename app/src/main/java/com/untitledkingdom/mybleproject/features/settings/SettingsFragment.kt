package com.untitledkingdom.mybleproject.features.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.findNavController
import com.tomcz.ellipse.common.onProcessor
import com.untitledkingdom.mybleproject.R
import com.untitledkingdom.mybleproject.features.settings.state.SettingsEffect
import com.untitledkingdom.mybleproject.features.welcome.BUNDLE_IS_SETTINGS_SCREEN
import com.untitledkingdom.mybleproject.utils.OnBackPressed
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@FlowPreview
@AndroidEntryPoint
class SettingsFragment : Fragment(), OnBackPressed {
    private val viewModel: SettingsViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        onProcessor(
            lifecycleState = Lifecycle.State.RESUMED,
            processor = { viewModel.processor },
            onEffect = ::trigger
        )

        return ComposeView(
            requireContext()
        ).apply {
            setContent {
                SettingsScreenComposable(
                    viewModel.processor
                )
            }
        }
    }

    private fun trigger(effect: SettingsEffect) {
        when (effect) {
            SettingsEffect.GoToSelectDeviceScreen -> goToSelectDeviceScreen()
        }
    }

    private fun goToSelectDeviceScreen() =
        findNavController().navigate(
            R.id.action_settingsFragment_to_welcomeFragment,
            Bundle().apply {
                putBoolean(BUNDLE_IS_SETTINGS_SCREEN, true)
            }
        )

    override fun onBackPressed(): Boolean = true
}
