package com.untitledkingdom.mybleproject.features.settings

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import com.tomcz.ellipse.common.previewProcessor
import com.untitledkingdom.mybleproject.R
import com.untitledkingdom.mybleproject.features.settings.state.SettingsEvent
import com.untitledkingdom.mybleproject.features.settings.state.SettingsState
import com.untitledkingdom.mybleproject.ui.theme.padding8

@Composable
fun SettingsScreenComposable(processor: SettingsProcessor) {
    val context = LocalContext.current
    Column {
        Button(
            modifier = Modifier.padding(all = padding8),
            onClick = { selectDeviceClicked(processor = processor) }
        ) {
            Text(text = context.getString(R.string.settings_select_device))
        }
    }
}

fun selectDeviceClicked(processor: SettingsProcessor) =
    processor.sendEvent(SettingsEvent.GoToSelectDeviceScreen)

@Preview(showBackground = true)
@Composable
private fun Preview() =
    SettingsScreenComposable(processor = previewProcessor(SettingsState))
