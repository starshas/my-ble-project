package com.untitledkingdom.mybleproject.features.settings

import androidx.lifecycle.ViewModel
import com.tomcz.ellipse.Processor
import com.tomcz.ellipse.common.processor
import com.tomcz.ellipse.common.toNoAction
import com.untitledkingdom.mybleproject.features.settings.state.SettingsEffect
import com.untitledkingdom.mybleproject.features.settings.state.SettingsEvent
import com.untitledkingdom.mybleproject.features.settings.state.SettingsState
import kotlinx.coroutines.FlowPreview

typealias SettingsProcessor = Processor<SettingsEvent, SettingsState, SettingsEffect>

class SettingsViewModel : ViewModel() {
    @FlowPreview
    val processor: SettingsProcessor = processor(
        initialState = SettingsState,
    ) { event: SettingsEvent ->
        when (event) {
            SettingsEvent.GoToSelectDeviceScreen -> effects.send(SettingsEffect.GoToSelectDeviceScreen)
                .toNoAction()
        }
    }
}
