package com.untitledkingdom.mybleproject.features.settings.state

sealed interface SettingsEffect {
    object GoToSelectDeviceScreen : SettingsEffect
}
