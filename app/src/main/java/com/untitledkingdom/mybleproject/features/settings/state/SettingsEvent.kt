package com.untitledkingdom.mybleproject.features.settings.state

sealed interface SettingsEvent {
    object GoToSelectDeviceScreen : SettingsEvent
}
