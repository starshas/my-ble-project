package com.untitledkingdom.mybleproject.features.settings.state

import com.tomcz.ellipse.PartialState

sealed interface SettingsPartialState : PartialState<SettingsState>
