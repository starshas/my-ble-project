package com.untitledkingdom.mybleproject.features.splash

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.findNavController
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.tomcz.ellipse.common.onProcessor
import com.untitledkingdom.mybleproject.R
import com.untitledkingdom.mybleproject.features.splash.state.SplashEffect
import com.untitledkingdom.mybleproject.utils.showToast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.FlowPreview

@ExperimentalPermissionsApi
@FlowPreview
@AndroidEntryPoint
class SplashFragment : Fragment() {
    private val viewModel: SplashViewModel by viewModels(ownerProducer = { requireActivity() })

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        onProcessor(
            lifecycleState = Lifecycle.State.RESUMED,
            processor = viewModel::processor,
            onEffect = ::trigger,
        )
        return ComposeView(
            requireContext()
        ).apply {
            setContent {
                SplashScreenComposable(viewModel.processor)
            }
        }
    }

    private fun trigger(effect: SplashEffect) {
        when (effect) {
            SplashEffect.GoToWelcomeScreen -> goToWelcomeScreen()
            SplashEffect.GoToMainScreen -> goToMainScreen()
            is SplashEffect.ShowToast -> requireActivity().applicationContext.showToast(
                resId = effect.resId,
                parameterString = effect.parameterString
            )
        }
    }

    private fun goToMainScreen() =
        findNavController().navigate(R.id.action_splashFragment_to_mainFragment)

    private fun goToWelcomeScreen() =
        findNavController().navigate(R.id.action_splashFragment_to_welcomeFragment)
}
