package com.untitledkingdom.mybleproject.features.splash

import android.Manifest
import android.os.Build
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.rememberMultiplePermissionsState
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.tomcz.ellipse.common.previewProcessor
import com.untitledkingdom.mybleproject.R
import com.untitledkingdom.mybleproject.features.splash.state.SplashEvent
import com.untitledkingdom.mybleproject.features.splash.state.SplashState
import com.untitledkingdom.mybleproject.ui.texts.TextWhite
import com.untitledkingdom.mybleproject.ui.theme.Colors
import com.untitledkingdom.mybleproject.ui.theme.fontSize24
import com.untitledkingdom.mybleproject.utils.allPermissionsPermanentlyDenied
import com.untitledkingdom.mybleproject.utils.atLeastOneIsPermanentlyDenied
import com.untitledkingdom.mybleproject.utils.isPermissionGranted
import kotlinx.coroutines.delay

private const val DELAY_GO_WELCOME_SCREEN_MILLIS = 2_500L

@ExperimentalPermissionsApi
@Composable
fun SplashScreenComposable(processor: SplashProcessor) {
    SetSystemBarColor()
    RequestPermissionIfNeed(processor)

    SplashText()
}

@ExperimentalPermissionsApi
@Composable
private fun RequestPermissionIfNeed(processor: SplashProcessor) =
    RequestPermissionsIfNeeded(
        processor = processor,
        listPermissions = getListPermissions(),
        callbackSuccess = {
            // Permission that is requested separately
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                val permissionBackgroundLocation = Manifest.permission.ACCESS_BACKGROUND_LOCATION
                if (permissionBackgroundLocation.isPermissionGranted()) {
                    processor.sendEvent(SplashEvent.ShowToast(R.string.splash_toast_allow_location_all_time))

                    RequestPermissionsIfNeeded(
                        processor = processor,
                        listPermissions = listOf(
                            permissionBackgroundLocation
                        ),
                        callbackSuccess = { LaunchedEffectGoToWelcomeScreen(processor = processor) }
                    )
                } else
                    LaunchedEffectGoToWelcomeScreen(processor = processor)
            } else
                LaunchedEffectGoToWelcomeScreen(processor = processor)
        }
    )

@Composable
private fun SplashText() =
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Colors.SplashPurple),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        TextWhite(
            text = LocalContext.current.getString(R.string.app_name),
            fontSize = fontSize24
        )
    }

private fun goToNextScreen(processor: SplashProcessor) =
    processor.sendEvent(SplashEvent.GoToNextScreen)

private fun getListPermissions(): List<String> {
    val listPermissions = mutableListOf(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION
    )
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
        listPermissions.addAll(
            listOf(
                Manifest.permission.BLUETOOTH_SCAN,
                Manifest.permission.BLUETOOTH_CONNECT
            )
        )
    }

    return listPermissions
}

@ExperimentalPermissionsApi
@Composable
private fun RequestPermissionsIfNeeded(
    processor: SplashProcessor,
    listPermissions: List<String>,
    callbackSuccess: @Composable () -> Unit,
) {
    val statePermissions = rememberMultiplePermissionsState(
        permissions = listPermissions
    )
    var hasRequestedPermissions by remember {
        mutableStateOf(false)
    }
    val lifecycleOwner = LocalLifecycleOwner.current
    DisposableEffect(
        key1 = lifecycleOwner,
        effect = {
            val observer = LifecycleEventObserver { _, event ->
                if (event == Lifecycle.Event.ON_RESUME) {
                    if (!hasRequestedPermissions) {
                        statePermissions.launchMultiplePermissionRequest()
                        hasRequestedPermissions = true
                    } else {
                        if (!statePermissions.allPermissionsPermanentlyDenied(permissionRequested = true)) {
                            statePermissions.launchMultiplePermissionRequest()
                        } else {
                            if (statePermissions.atLeastOneIsPermanentlyDenied())
                                processor.sendEvent(
                                    SplashEvent.ShowToast(
                                        R.string.splash_toast_enable_permissions_in_settings
                                    )
                                )
                        }
                    }
                }
            }
            val lifecycle = lifecycleOwner.lifecycle
            lifecycle.addObserver(observer)

            onDispose {
                lifecycle.removeObserver(observer)
            }
        }
    )
    if (statePermissions.allPermissionsGranted) {
        callbackSuccess()
    }
}

@Composable
private fun SetSystemBarColor() =
    rememberSystemUiController().setSystemBarsColor(
        color = Colors.SplashPurple
    )

@Composable
private fun LaunchedEffectGoToWelcomeScreen(processor: SplashProcessor) =
    LaunchedEffect(
        key1 = true, block = {
            delay(DELAY_GO_WELCOME_SCREEN_MILLIS)
            goToNextScreen(processor = processor)
        }
    )

@ExperimentalPermissionsApi
@Preview(showBackground = true)
@Composable
private fun Preview() =
    SplashScreenComposable(previewProcessor(SplashState))
