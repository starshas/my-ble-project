package com.untitledkingdom.mybleproject.features.splash

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tomcz.ellipse.Processor
import com.tomcz.ellipse.common.processor
import com.tomcz.ellipse.common.toNoAction
import com.untitledkingdom.mybleproject.bluetooth.CustomBluetoothManager
import com.untitledkingdom.mybleproject.datastore.DataStorage
import com.untitledkingdom.mybleproject.datastore.DataStorageConstants
import com.untitledkingdom.mybleproject.features.splash.state.SplashEffect
import com.untitledkingdom.mybleproject.features.splash.state.SplashEvent
import com.untitledkingdom.mybleproject.features.splash.state.SplashState
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

typealias SplashProcessor = Processor<SplashEvent, SplashState, SplashEffect>

@HiltViewModel
class SplashViewModel @Inject constructor(
    private val dataStorage: DataStorage,
    private val customBluetoothManager: CustomBluetoothManager
) : ViewModel() {
    val processor: SplashProcessor = processor(
        initialState = SplashState,
        prepare = {
            connectToSavedDeviceIfCan().toNoAction()
        },
        onEvent = { event ->
            when (event) {
                SplashEvent.GoToNextScreen -> {
                    if (customBluetoothManager.isConnected)
                        effects.send(SplashEffect.GoToMainScreen).toNoAction()
                    else {
                        customBluetoothManager.disconnect()
                        effects.send(SplashEffect.GoToWelcomeScreen).toNoAction()
                    }
                }
                is SplashEvent.ShowToast -> effects.send(
                    SplashEffect.ShowToast(
                        resId = event.resId,
                        parameterString = event.parameterString
                    )
                ).toNoAction()
            }
        }
    )

    private suspend fun connectToSavedDeviceIfCan() {
        val savedDeviceMac = getSavedDeviceMac()
        if (savedDeviceMac.isNotEmpty()) {
            customBluetoothManager.connect(
                scope = viewModelScope,
                identifier = savedDeviceMac
            )
        }
    }

    private suspend fun getSavedDeviceMac(): String =
        dataStorage.getFromStorage(DataStorageConstants.KEY_NAME_SAVED_DEVICE_IDENTIFIER)
}
