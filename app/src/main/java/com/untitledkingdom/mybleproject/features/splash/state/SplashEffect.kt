package com.untitledkingdom.mybleproject.features.splash.state

sealed interface SplashEffect {
    data class ShowToast(
        val resId: Int,
        val parameterString: String? = null
    ) : SplashEffect

    object GoToMainScreen : SplashEffect
    object GoToWelcomeScreen : SplashEffect
}
