package com.untitledkingdom.mybleproject.features.splash.state

sealed interface SplashEvent {
    data class ShowToast(
        val resId: Int,
        val parameterString: String? = null
    ) : SplashEvent

    object GoToNextScreen : SplashEvent
}
