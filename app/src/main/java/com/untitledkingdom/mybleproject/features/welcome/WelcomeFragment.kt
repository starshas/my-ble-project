package com.untitledkingdom.mybleproject.features.welcome

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.IntentSender
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.findNavController
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsResponse
import com.google.android.gms.location.LocationSettingsStatusCodes
import com.google.android.gms.tasks.Task
import com.tomcz.ellipse.common.onProcessor
import com.untitledkingdom.mybleproject.R
import com.untitledkingdom.mybleproject.background.ForegroundServiceManager
import com.untitledkingdom.mybleproject.bluetooth.CustomBluetoothManager
import com.untitledkingdom.mybleproject.features.welcome.state.WelcomeEffect
import com.untitledkingdom.mybleproject.features.welcome.state.WelcomeEvent
import com.untitledkingdom.mybleproject.utils.OnBackPressed
import com.untitledkingdom.mybleproject.utils.showToast
import com.untitledkingdom.mybleproject.utils.toBooleanOrFalse
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.flowOf
import timber.log.Timber
import javax.inject.Inject

const val BUNDLE_IS_SETTINGS_SCREEN = "BUNDLE_IS_SETTING_SCREEN"

@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@FlowPreview
@AndroidEntryPoint
class WelcomeFragment : Fragment(), OnBackPressed {
    private var hasLocationAccess: Boolean = false
    private var isBluetoothTurnedOn: Boolean = false
    private val viewModel: WelcomeViewModel by viewModels()

    @Inject
    lateinit var customBluetoothManager: CustomBluetoothManager

    @Inject
    lateinit var foregroundServiceManager: ForegroundServiceManager

    private val broadcastReceiver = BluetoothBroadcastReceiver()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        onProcessor(
            lifecycleState = Lifecycle.State.RESUMED,
            processor = { viewModel.processor },
            onEffect = ::trigger,
            viewEvents = ::viewEvents
        )
        showLocationPrompt()
        val isSettingsScreen: Boolean =
            arguments?.getBoolean(BUNDLE_IS_SETTINGS_SCREEN).toBooleanOrFalse()
        return ComposeView(
            requireContext()
        ).apply {
            setContent {
                WelcomeScreenComposable(viewModel.processor, isSettingsScreen)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        foregroundServiceManager.startService(this::class.java)
    }

    override fun onResume() {
        super.onResume()
        registerBroadcastReceiver()
        val backQueue = findNavController().backQueue
        Timber.d("backQueue ----------")
        for (item in backQueue)
            Timber.d("backQueue = ${item.destination.displayName}")
    }

    private fun viewEvents() = listOf(
        flowOf(
            WelcomeEvent.TurnOnBluetooth
        )
    )

    private fun registerBroadcastReceiver() {
        val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        requireContext().registerReceiver(broadcastReceiver, filter)
    }

    private fun trigger(effect: WelcomeEffect) {
        val context = requireActivity().applicationContext

        when (effect) {
            is WelcomeEffect.GoToMainScreen -> {
                goToMainScreen()
            }
            is WelcomeEffect.ShowToast -> context.showToast(
                resId = effect.resId,
                parameterString = effect.parameterString
            )
            is WelcomeEffect.ShowToastText -> context.showToast(effect.text)
        }
    }

    private fun goToMainScreen() {
        findNavController().popBackStack(R.id.mainFragment, true)
        findNavController().popBackStack(R.id.welcomeFragment, true)
        findNavController().navigate(R.id.mainFragment)
    }

    @FlowPreview
    private inner class BluetoothBroadcastReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val action = intent?.action ?: return
            if (action == BluetoothAdapter.ACTION_STATE_CHANGED) {
                when (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR)) {
                    BluetoothAdapter.STATE_ON -> {
                        Timber.d("BluetoothBroadcastReceiver STATE_ON")
                        isBluetoothTurnedOn = true
                        if (hasLocationAccess)
                            sendEventBluetoothTurnedOn()
                    }
                    BluetoothAdapter.STATE_OFF -> {
                        isBluetoothTurnedOn = false
                        Timber.d("BluetoothBroadcastReceiver STATE_OFF")
                        sendEventBluetoothTurnedOff()
                    }
                }
            }
        }
    }

    private fun showLocationPrompt() {
        val locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)

        val result: Task<LocationSettingsResponse> =
            LocationServices.getSettingsClient(requireActivity())
                .checkLocationSettings(builder.build())

        result.addOnCompleteListener { task ->
            try {
                task.getResult(ApiException::class.java)
                // All location settings are satisfied. The client can initialize location
                // requests here.
                Timber.d("Already has location access")
                hasLocationAccess = true
                if (isBluetoothTurnedOn || isBluetoothOn)
                    sendEventBluetoothTurnedOn()
            } catch (exception: ApiException) {
                when (exception.statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                        try {
                            Timber.d("LocationSettingsStatusCodes.RESOLUTION_REQUIRED")
                            val reasonable = exception as ResolvableApiException
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            reasonable.startResolutionForResult(
                                requireActivity(), LocationRequest.PRIORITY_HIGH_ACCURACY
                            )
                        } catch (e: IntentSender.SendIntentException) {
                            Timber.d("IntentSender.SendIntentException")
                            // Ignore the error.
                        } catch (e: ClassCastException) {
                            Timber.d("ClassCastException")
                            // Ignore, should be an impossible error.
                        }
                    }
                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                        Timber.d("LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE")
                        // Location settings are not satisfied. But could be fixed by showing the
                        // user a dialog.

                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Timber.d("onActivityResult")
        when (requestCode) {
            LocationRequest.PRIORITY_HIGH_ACCURACY -> {
                if (resultCode == Activity.RESULT_OK) {
                    Timber.d("Status: On")
                    hasLocationAccess = true
                    if (isBluetoothTurnedOn || isBluetoothOn)
                        sendEventBluetoothTurnedOn()
                } else {
                    hasLocationAccess = false
                    Timber.d("Status: Off")
                }
            }
        }
    }

    private val isBluetoothOn: Boolean
        get() = customBluetoothManager.isBluetoothEnabled

    override fun onPause() {
        super.onPause()
        requireContext().unregisterReceiver(broadcastReceiver)
    }

    private fun sendEventBluetoothTurnedOn() =
        viewModel.processor.sendEvent(WelcomeEvent.BluetoothTurnedOn)

    private fun sendEventBluetoothTurnedOff() =
        viewModel.processor.sendEvent(WelcomeEvent.BluetoothTurnedOff)

    override fun onBackPressed() = true
}
