package com.untitledkingdom.mybleproject.features.welcome

import com.juul.kable.Advertisement
import kotlinx.coroutines.flow.Flow

interface WelcomeRepository {
    fun getAdvertisements(): Flow<Advertisement>
}
