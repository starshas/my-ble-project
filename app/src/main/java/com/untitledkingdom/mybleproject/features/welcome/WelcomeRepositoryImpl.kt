package com.untitledkingdom.mybleproject.features.welcome

import com.juul.kable.Advertisement
import com.juul.kable.Scanner
import com.untitledkingdom.mybleproject.utils.isEligibleTag
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filter
import javax.inject.Inject

class WelcomeRepositoryImpl @Inject constructor(
    private val scanner: Scanner
) : WelcomeRepository {
    override fun getAdvertisements(): Flow<Advertisement> =
        scanner.advertisements.filter { it.isEligibleTag() }
}
