package com.untitledkingdom.mybleproject.features.welcome

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Divider
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import com.juul.kable.Advertisement
import com.tomcz.ellipse.common.collectAsState
import com.tomcz.ellipse.common.previewProcessor
import com.untitledkingdom.mybleproject.BuildConfig
import com.untitledkingdom.mybleproject.R
import com.untitledkingdom.mybleproject.features.welcome.data.AppInfoData
import com.untitledkingdom.mybleproject.features.welcome.state.WelcomeEvent
import com.untitledkingdom.mybleproject.features.welcome.state.WelcomeState
import com.untitledkingdom.mybleproject.ui.theme.padding16
import com.untitledkingdom.mybleproject.utils.getNameOrAddress
import timber.log.Timber

@Composable
fun WelcomeScreenComposable(processor: WelcomeProcessor, isSettingsScreen: Boolean) {
    Timber.d("isSettingsScreen $isSettingsScreen")
    turnOnBluetooth(processor = processor)

    Column(modifier = Modifier.fillMaxWidth()) {
        if (!isSettingsScreen)
            ApplicationInfo()
        DevicesList(processor = processor)
    }
}

@Composable
private fun ApplicationInfo() {
    val appInfo = getAppInfo()
    Text(
        modifier = Modifier.padding(
            top = padding16,
            bottom = padding16,
            start = padding16
        ),
        text = "${appInfo.appName} ${appInfo.appVersion} ${appInfo.createdBy}",
        fontWeight = FontWeight.Bold
    )
}

@Composable
fun getAppInfo(): AppInfoData {
    val context = LocalContext.current
    val appName = context.getString(R.string.app_name)
    val appVersion = BuildConfig.VERSION_NAME
    val createdBy = context.getString(R.string.created_by)

    return AppInfoData(
        appName = appName,
        appVersion = appVersion,
        createdBy = createdBy
    )
}

private fun turnOnBluetooth(processor: WelcomeProcessor) {
    processor.sendEvent(WelcomeEvent.TurnOnBluetooth)
}

@Composable
private fun DevicesList(processor: WelcomeProcessor) {
    val listAdvertisements by processor.collectAsState { it.listAdvertisements }

    LazyColumn {
        val count = listAdvertisements.size
        items(count) { index ->
            val advertisement = listAdvertisements[index]
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .clickable {
                        deviceClicked(
                            processor = processor,
                            advertisement = advertisement
                        )
                    }
            ) {
                Column {
                    Row(
                        modifier = Modifier.padding(
                            top = padding16,
                            start = padding16,
                            bottom = padding16
                        )
                    ) {
                        Text(text = advertisement.getNameOrAddress())
                    }
                    Divider()
                }
            }
        }
    }
}

fun deviceClicked(
    processor: WelcomeProcessor,
    advertisement: Advertisement
) =
    processor.sendEvent(WelcomeEvent.ConnectToDevice(advertisement))

@Preview(showBackground = true)
@Composable
private fun Preview() =
    WelcomeScreenComposable(
        processor = previewProcessor(WelcomeState()),
        isSettingsScreen = true
    )
