package com.untitledkingdom.mybleproject.features.welcome

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.juul.kable.Advertisement
import com.tomcz.ellipse.EffectsCollector
import com.tomcz.ellipse.PartialState
import com.tomcz.ellipse.Processor
import com.tomcz.ellipse.common.NoAction
import com.tomcz.ellipse.common.processor
import com.tomcz.ellipse.common.toNoAction
import com.untitledkingdom.mybleproject.R
import com.untitledkingdom.mybleproject.bluetooth.CustomBluetoothManager
import com.untitledkingdom.mybleproject.datastore.DataStorage
import com.untitledkingdom.mybleproject.datastore.DataStorageConstants
import com.untitledkingdom.mybleproject.features.welcome.state.WelcomeEffect
import com.untitledkingdom.mybleproject.features.welcome.state.WelcomeEvent
import com.untitledkingdom.mybleproject.features.welcome.state.WelcomePartialState
import com.untitledkingdom.mybleproject.features.welcome.state.WelcomeState
import com.untitledkingdom.mybleproject.utils.getNameOrAddress
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.takeWhile
import timber.log.Timber
import javax.inject.Inject

typealias WelcomeProcessor = Processor<WelcomeEvent, WelcomeState, WelcomeEffect>

@HiltViewModel
class WelcomeViewModel @Inject constructor(
    private val customBluetoothManager: CustomBluetoothManager,
    private val dataStorage: DataStorage,
    private val repository: WelcomeRepository
) : ViewModel() {
    private var scanningStatus: ScanStatus = ScanStatus.Stopped
    private val isScanning: Boolean
        get() = scanningStatus == ScanStatus.Scanning
    private val isConnecting: Boolean
        get() = customBluetoothManager.isConnecting

    @FlowPreview
    val processor: WelcomeProcessor = processor(
        initialState = WelcomeState()
    ) { event: WelcomeEvent ->
        when (event) {
            is WelcomeEvent.TurnOnBluetooth -> turnOnBluetooth().toNoAction()
            is WelcomeEvent.BluetoothTurnedOn -> handleBluetoothTurnedOn()
            is WelcomeEvent.BluetoothTurnedOff -> handleBluetoothTurnedOff().toNoAction()
            is WelcomeEvent.ConnectToDevice -> handleDeviceConnect(
                event = event,
                effects = effects
            ).toNoAction()
        }
    }

    private suspend fun handleDeviceConnect(
        event: WelcomeEvent.ConnectToDevice,
        effects: EffectsCollector<WelcomeEffect>,
    ) {
        if (!isConnecting) {
            startConnect(effects, event)
        } else {
            effects.send(
                WelcomeEffect.ShowToast(
                    resId = R.string.welcome_disconnecting,
                )
            )
            customBluetoothManager.disconnect()
            effects.send(
                WelcomeEffect.ShowToast(
                    resId = R.string.welcome_disconnected,
                )
            )
            startConnect(effects, event)
        }
    }

    private suspend fun startConnect(
        effects: EffectsCollector<WelcomeEffect>,
        event: WelcomeEvent.ConnectToDevice
    ) {
        val deviceNameOrAddress = event.advertisement.getNameOrAddress()
        effects.send(
            WelcomeEffect.ShowToast(
                resId = R.string.welcome_connecting_to,
                parameterString = deviceNameOrAddress
            )
        )
        return connectToDevice(
            advertisement = event.advertisement,
            callbackConnected = {
                effects.send(WelcomeEffect.ShowToast(R.string.welcome_connected))
                effects.send(WelcomeEffect.GoToMainScreen)
            },
            callbackError = { _, errorMessage ->
                effects.send(WelcomeEffect.ShowToastText(errorMessage))
            }
        )
    }

    private suspend fun connectToDevice(
        advertisement: Advertisement,
        callbackConnected: () -> Unit = {},
        callbackError: (exception: Exception, errorMessage: String) -> Unit = { _, _ -> }
    ) = customBluetoothManager.connect(
        scope = viewModelScope,
        advertisement = advertisement,
        callbackConnected = callbackConnected.also {
            dataStorage.saveToStorage(
                DataStorageConstants.KEY_NAME_SAVED_DEVICE_IDENTIFIER,
                advertisement.address
            )
        },
        callbackError = callbackError
    )

    @FlowPreview
    private fun handleBluetoothTurnedOff() {
        if (isScanning)
            stopScan()
    }

    private fun stopScan() {
        scanningStatus = ScanStatus.Stopped
    }

    @FlowPreview
    private fun handleBluetoothTurnedOn(): Flow<PartialState<WelcomeState>> {
        Timber.d("handleBluetoothTurnedOn isScanning = $isScanning")
        return startScanIfNotScanning()
    }

    private fun turnOnBluetooth() = customBluetoothManager.turnOnBluetooth()

    @FlowPreview
    private fun startScanIfNotScanning(): Flow<PartialState<WelcomeState>> {
        if (isScanning) return emptyFlow()
        setScanStatusScanning()

        Timber.d("entered startScan")
        return repository.getAdvertisements().map { scannedAdvertisement ->
            val isAdvertisementAdded =
                isAdvertisementAdded(scannedAdvertisement)

            if (!isAdvertisementAdded)
                WelcomePartialState.ScannedDeviceAdded(scannedAdvertisement)
            else
                NoAction()
        }.takeWhile { isScanning }
            .catch { cause ->
                setScanStatusFailed(cause.message ?: "Unknown error")
            }
            .onCompletion { cause ->
                if (cause == null || cause is CancellationException)
                    stopScan()
            }
    }

    private fun setScanStatusFailed(causeMessage: String) {
        scanningStatus = ScanStatus.Failed(causeMessage)
    }

    private fun setScanStatusScanning() {
        scanningStatus = ScanStatus.Scanning
    }

    @FlowPreview
    private fun isAdvertisementAdded(
        scannerAdvertisement: Advertisement
    ): Boolean {
        val listExistingAdvertisements = processor.state.value.listAdvertisements
        return listExistingAdvertisements.map { it.address }.contains(scannerAdvertisement.address)
    }

    sealed class ScanStatus {
        object Stopped : ScanStatus()
        object Scanning : ScanStatus()
        data class Failed(val message: String) : ScanStatus()
    }
}
