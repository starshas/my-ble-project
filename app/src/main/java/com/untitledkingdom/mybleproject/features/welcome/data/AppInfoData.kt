package com.untitledkingdom.mybleproject.features.welcome.data

class AppInfoData(
    val appName: String,
    val appVersion: String,
    val createdBy: String
)
