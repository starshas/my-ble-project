package com.untitledkingdom.mybleproject.features.welcome.state

sealed interface WelcomeEffect {
    data class ShowToast(
        val resId: Int,
        val parameterString: String? = null
    ) : WelcomeEffect
    data class ShowToastText(val text: String) : WelcomeEffect
    object GoToMainScreen : WelcomeEffect
}
