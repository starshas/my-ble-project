package com.untitledkingdom.mybleproject.features.welcome.state

import com.juul.kable.Advertisement

sealed interface WelcomeEvent {
    data class ConnectToDevice(val advertisement: Advertisement) : WelcomeEvent
    object TurnOnBluetooth : WelcomeEvent
    object BluetoothTurnedOn : WelcomeEvent
    object BluetoothTurnedOff : WelcomeEvent
}
