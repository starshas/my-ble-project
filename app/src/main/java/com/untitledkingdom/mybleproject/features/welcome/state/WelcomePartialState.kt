package com.untitledkingdom.mybleproject.features.welcome.state

import com.juul.kable.Advertisement
import com.tomcz.ellipse.PartialState

sealed interface WelcomePartialState : PartialState<WelcomeState> {
    data class ScannedDeviceAdded(val advertisement: Advertisement) : WelcomePartialState {
        override fun reduce(oldState: WelcomeState) =
            oldState.copy(
                listAdvertisements = oldState.listAdvertisements + advertisement
            )
    }
}
