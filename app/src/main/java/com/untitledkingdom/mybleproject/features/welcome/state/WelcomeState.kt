package com.untitledkingdom.mybleproject.features.welcome.state

import com.juul.kable.Advertisement

data class WelcomeState(
    val listAdvertisements: List<Advertisement> = listOf()
)
