package com.untitledkingdom.mybleproject.ui.spacers

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

@Composable
private fun SpacerHeight(heightDp: Float) =
    Spacer(modifier = Modifier.height(height = heightDp.dp))

@Composable
fun SpacerHeight(height: Dp) =
    SpacerHeight(heightDp = height.value)

@Composable
private fun SpacerWidth(widthDp: Float) =
    Spacer(modifier = Modifier.width(width = widthDp.dp))

@Composable
fun SpacerWidth(width: Dp) =
    SpacerWidth(widthDp = width.value)
