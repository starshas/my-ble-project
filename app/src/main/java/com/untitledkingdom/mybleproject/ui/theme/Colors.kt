package com.untitledkingdom.mybleproject.ui.theme

import androidx.compose.ui.graphics.Color

object Colors {
    val Purple = Color(0xFF9E87F5)
    val SplashPurple = Color(0xFF9E87F5)
    val Black = Color(0xFF000000)
    val Purple200 = Color(0xFFBB86FC)
    val Purple500 = Color(0xFF6200EE)
    val Purple700 = Color(0xFF3700B3)
    val secondaryVariant: Color = Color(0xff9E87F5)
    val Orange = Color(0xFFFFA500)
    val LightBlue = Color(0xFF0288d1)
}
