package com.untitledkingdom.mybleproject.ui.theme

import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

val padding2: Dp = 2.dp
val padding4: Dp = 4.dp
val padding6: Dp = 6.dp
val padding8: Dp = 8.dp
val padding10: Dp = 10.dp
val padding12: Dp = 12.dp
val padding14: Dp = 14.dp
val padding16: Dp = 16.dp
val padding18: Dp = 18.dp
val padding24: Dp = 24.dp
val padding32: Dp = 32.dp
val padding36: Dp = 36.dp
val padding42: Dp = 42.dp
val padding34: Dp = 34.dp
val padding48: Dp = 48.dp
val padding64: Dp = 64.dp
val padding72: Dp = 72.dp
