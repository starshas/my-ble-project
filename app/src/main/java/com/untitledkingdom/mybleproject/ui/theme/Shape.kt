package com.untitledkingdom.mybleproject.ui.theme

import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Shapes
import androidx.compose.ui.unit.dp

val Shapes = Shapes(
    small = RoundedCornerShape(0.dp),
    medium = RoundedCornerShape(8.dp),
    large = RoundedCornerShape(32.dp)
)
val shape4 = RoundedCornerShape(4.dp)
val shape8 = RoundedCornerShape(8.dp)
val shape12 = RoundedCornerShape(12.dp)
val shape16 = RoundedCornerShape(16.dp)
val shape24 = RoundedCornerShape(24.dp)
val shape32 = RoundedCornerShape(32.dp)
