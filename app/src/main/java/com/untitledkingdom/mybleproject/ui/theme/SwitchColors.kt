package com.untitledkingdom.mybleproject.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

@Composable
fun switchColors(colorLightMode: Color, colorDarkMode: Color): Color {
    return if (isSystemInDarkTheme())
        colorDarkMode
    else
        colorLightMode
}
