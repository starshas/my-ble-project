package com.untitledkingdom.mybleproject.ui.theme

import androidx.compose.material.darkColors
import androidx.compose.material.lightColors

private val DarkColorPalette = darkColors(
    primary = Colors.Purple,
    primaryVariant = Colors.Purple700,
    secondary = Colors.Purple200,
    secondaryVariant = Colors.secondaryVariant,
    onPrimary = Colors.Black
)

private val LightColorPalette = lightColors(
    primary = Colors.Purple,
    primaryVariant = Colors.Purple700,
    secondary = Colors.Purple200,
    secondaryVariant = Colors.secondaryVariant,
    onPrimary = Colors.Black,

    /* Other default colors to override
    background = Colors.White,
    surface = Colors.White,
    onPrimary = Colors.White,
    onSecondary = Colors.Black,
    onBackground = Colors.Black,
    onSurface = Colors.Black,
    */
)
