package com.untitledkingdom.mybleproject.ui.theme

import androidx.compose.material.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp

// Set of Material typography styles to start with
val Typography = Typography(
    body1 = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Normal,
        fontSize = 16.sp
    )
)

val h7: TextStyle = TextStyle(
    fontWeight = FontWeight.Medium,
    fontSize = 18.sp,
    letterSpacing = 0.15.sp
)

val h7Normal: TextStyle = TextStyle(
    fontWeight = FontWeight.Normal,
    fontSize = 18.sp,
    letterSpacing = 0.15.sp
)

val textStyleToolBar: TextStyle = TextStyle(
    fontWeight = FontWeight.Bold,
    fontSize = 18.sp,
    letterSpacing = 0.15.sp
)
