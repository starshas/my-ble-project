package com.untitledkingdom.mybleproject.utils

import android.annotation.SuppressLint
import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.widget.Toast
import androidx.compose.runtime.Composable
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.MultiplePermissionsState
import com.google.accompanist.permissions.PermissionState
import com.google.accompanist.permissions.PermissionStatus
import com.google.accompanist.permissions.isGranted
import com.google.accompanist.permissions.rememberPermissionState
import com.google.accompanist.permissions.shouldShowRationale
import com.juul.kable.Advertisement
import com.juul.kable.Peripheral
import com.juul.kable.identifier
import com.untitledkingdom.mybleproject.bluetooth.ReadingConstants
import kotlinx.coroutines.flow.FlowCollector
import java.util.UUID

fun Context.showToast(text: String) =
    Toast.makeText(this, text, Toast.LENGTH_SHORT).show()

fun Context.showToast(resId: Int, parameterString: String?) =
    showToast(text = this.getString(resId, parameterString))

fun Advertisement.getNameOrAddress() = this.name ?: this.address

fun Any?.toStringOrEmpty(): String = this?.toString().orEmpty()

suspend fun <T> FlowCollector<T>.emitAll(vararg values: T) {
    for (item in values)
        emit(item)
}

fun String.toUuid(): UUID = UUID.fromString(this)

fun <E> Collection<E>.containsAny(vararg values: E): Boolean {
    for (item in values)
        if (this.contains(item))
            return true
    return false
}

fun Advertisement.isEligibleTag() =
    this.uuids.containsAny(
        ReadingConstants.UUID_SERVICE_READINGS_TIME.toUuid(),
        ReadingConstants.UUID_SERVICE_READINGS.toUuid(),
        ReadingConstants.UUID_SERVICE_BATTERY.toUuid()
    )

@SuppressLint("MissingPermission")
fun Peripheral.getDeviceName(bluetoothAdapter: BluetoothAdapter): String =
    bluetoothAdapter.getRemoteDevice(this.identifier).name

fun Number.toByteArray(size: Int) =
    ByteArray(size) { index -> (this.toLong() shr (index * Byte.SIZE_BITS)).toByte() }

fun Boolean?.toBooleanOrFalse() = this ?: false

@ExperimentalPermissionsApi
fun PermissionState.isPermanentlyDenied(isPermissionRequested: Boolean) =
    !status.shouldShowRationale && !status.isGranted && isPermissionRequested

@ExperimentalPermissionsApi
fun MultiplePermissionsState.atLeastOneIsPermanentlyDenied() =
    permissions.firstOrNull {
        it.isPermanentlyDenied(isPermissionRequested = true)
    } != null

@ExperimentalPermissionsApi
fun MultiplePermissionsState.allPermissionsPermanentlyDenied(
    permissionRequested: Boolean
): Boolean {
    for (item in permissions) {
        if (!item.isPermanentlyDenied(permissionRequested))
            return false
    }
    return true
}

@ExperimentalPermissionsApi
@Composable
fun String.isPermissionGranted() =
    rememberPermissionState(permission = this).status != PermissionStatus.Granted
