package com.untitledkingdom.mybleproject.utils

import android.app.Application
import android.app.NotificationManager
import android.bluetooth.BluetoothManager
import android.content.Context
import android.net.ConnectivityManager
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import androidx.room.Room
import com.juul.kable.Scanner
import com.juul.kable.logs.Logging
import com.juul.kable.logs.SystemLogEngine
import com.untitledkingdom.mybleproject.api.ApiService
import com.untitledkingdom.mybleproject.api.ApiServiceImpl
import com.untitledkingdom.mybleproject.background.ForegroundServiceManager
import com.untitledkingdom.mybleproject.background.ForegroundServiceManagerImpl
import com.untitledkingdom.mybleproject.bluetooth.CustomBluetoothManager
import com.untitledkingdom.mybleproject.bluetooth.CustomBluetoothManagerImpl
import com.untitledkingdom.mybleproject.database.AppDatabase
import com.untitledkingdom.mybleproject.database.DatabaseConstants
import com.untitledkingdom.mybleproject.datastore.DataStorage
import com.untitledkingdom.mybleproject.datastore.DataStorageConstants
import com.untitledkingdom.mybleproject.datastore.DataStorageImpl
import com.untitledkingdom.mybleproject.features.daydetails.DayDetailsRepository
import com.untitledkingdom.mybleproject.features.daydetails.DayDetailsRepositoryImpl
import com.untitledkingdom.mybleproject.features.history.HistoryRepository
import com.untitledkingdom.mybleproject.features.history.HistoryRepositoryImpl
import com.untitledkingdom.mybleproject.features.welcome.WelcomeRepository
import com.untitledkingdom.mybleproject.features.welcome.WelcomeRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object Modules {
    private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(
        name = DataStorageConstants.DATA_STORE_NAME
    )

    @Provides
    @Singleton
    fun provideDataStorage(dataStorageImpl: DataStorageImpl): DataStorage = dataStorageImpl

    @Provides
    @Singleton
    fun provideDataStore(context: Application): DataStore<Preferences> = context.dataStore

    @Provides
    @Singleton
    fun provideDataBase(context: Application): AppDatabase =
        Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            DatabaseConstants.DATABASE_NAME
        ).fallbackToDestructiveMigration().build()

    @Provides
    @Singleton
    fun getBluetoothManager(@ApplicationContext applicationContext: Context): BluetoothManager =
        applicationContext.getSystemService(BluetoothManager::class.java)

    @Provides
    @Singleton
    fun getNotificationManager(@ApplicationContext applicationContext: Context): NotificationManager =
        applicationContext.getSystemService(NotificationManager::class.java)

    @ExperimentalCoroutinesApi
    @ExperimentalUnsignedTypes
    @FlowPreview
    @Provides
    @Singleton
    fun getForegroundServiceManager(
        @ApplicationContext applicationContext: Context
    ): ForegroundServiceManager = ForegroundServiceManagerImpl(applicationContext)

    @Provides
    fun getScanner(): Scanner = Scanner {
        filters = null
        logging {
            engine = SystemLogEngine
            level = Logging.Level.Warnings
            format = Logging.Format.Multiline
        }
    }

    @Provides
    @Singleton
    fun getConnectivityManager(
        @ApplicationContext applicationContext: Context
    ): ConnectivityManager = applicationContext.getSystemService(ConnectivityManager::class.java)
}

@Module
@InstallIn(SingletonComponent::class)
interface CustomBluetoothManagerModule {
    @Binds
    fun bindCustomBluetoothManager(
        customBluetoothManagerImpl: CustomBluetoothManagerImpl,
    ): CustomBluetoothManager
}

@Module
@InstallIn(SingletonComponent::class)
interface ApplicationModule {
    @Binds
    fun bindApiService(
        apiServiceImpl: ApiServiceImpl
    ): ApiService

    @Binds
    fun bindWelcomeRepository(
        welcomeRepositoryImpl: WelcomeRepositoryImpl,
    ): WelcomeRepository

    @Binds
    fun bindHistoryRepository(
        historyRepositoryImpl: HistoryRepositoryImpl,
    ): HistoryRepository

    @Binds
    fun bindDayDetailsRepository(
        dayDetailsRepositoryImpl: DayDetailsRepositoryImpl,
    ): DayDetailsRepository
}
