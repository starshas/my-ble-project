package com.untitledkingdom.mybleproject.utils

interface OnBackPressed {
    // If true executes super.onBackPressed() in Activity
    fun onBackPressed(): Boolean
}
