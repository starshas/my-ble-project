package com.untitledkingdom.mybleproject.utils.date

import java.time.LocalDate
import java.time.OffsetDateTime

object DateFormatter {
    fun OffsetDateTime.formatToDate(): String =
        DatePattern.date.format(this)

    fun OffsetDateTime.formatToDateAndTime(): String =
        DatePattern.dateAndTimeWithSeconds.format(this)

    fun LocalDate.basicFormat(): String = this.format(DatePattern.readingsTime)
}
