package com.untitledkingdom.mybleproject.utils.date

import java.time.format.DateTimeFormatter
import java.util.Locale

object DatePattern {
    val date: DateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy", Locale.ENGLISH)
    val dateAndTimeWithSeconds: DateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss", Locale.ENGLISH)
    val readingsTime: DateTimeFormatter =
        DateTimeFormatter.ofPattern("dd/MM/yyyy", Locale.ENGLISH)
    val timeWithSeconds: DateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss", Locale.ENGLISH)
}
