package com.untitledkingdom.mybleproject.utils.date

import java.time.LocalDateTime

interface TimeManager {
    fun getCurrentTime(): String
    fun provideCurrentLocalDateTime(): LocalDateTime
}
