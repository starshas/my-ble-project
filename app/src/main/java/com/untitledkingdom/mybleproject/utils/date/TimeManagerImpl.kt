package com.untitledkingdom.mybleproject.utils.date

import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId

class TimeManagerImpl : TimeManager {
    private fun provideCurrentInstant(): Instant = Instant.now()

    private fun provideCurrentZone(): ZoneId = ZoneId.systemDefault()

    override fun provideCurrentLocalDateTime(): LocalDateTime =
        LocalDateTime.ofInstant(provideCurrentInstant(), provideCurrentZone())

    override fun getCurrentTime(): String =
        LocalDateTime.ofInstant(provideCurrentInstant(), provideCurrentZone())
            .format(DatePattern.readingsTime)
}
