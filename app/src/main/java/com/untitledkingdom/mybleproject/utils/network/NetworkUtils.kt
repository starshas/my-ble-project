package com.untitledkingdom.mybleproject.utils.network

import android.net.ConnectivityManager
import android.net.NetworkCapabilities

object NetworkUtils {
    fun ConnectivityManager.isOnline(): Boolean {
        val capabilities = this.getNetworkCapabilities(this.activeNetwork)
        if (capabilities != null) {
            when {
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                    return true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                    return true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                    return true
                }
            }
        }
        return false
    }
}
