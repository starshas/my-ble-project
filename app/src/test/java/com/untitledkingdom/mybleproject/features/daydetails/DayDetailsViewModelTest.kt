package com.untitledkingdom.mybleproject.features.daydetails

import com.tomcz.ellipse.test.processorTest
import com.untitledkingdom.mybleproject.database.data.ReadingsRecord
import com.untitledkingdom.mybleproject.features.daydetails.state.DayDetailsEffect
import com.untitledkingdom.mybleproject.features.daydetails.state.DayDetailsEvent
import com.untitledkingdom.mybleproject.features.daydetails.state.DayDetailsState
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.time.OffsetDateTime

@FlowPreview
@ExperimentalCoroutinesApi
internal class DayDetailsViewModelTest {
    private val repository by lazy { mockk<DayDetailsRepository>() }
    private val dispatcher = StandardTestDispatcher()

    private val viewModel: DayDetailsViewModel by lazy {
        DayDetailsViewModel(
            repository = repository
        )
    }

    @Before
    fun setup() = Dispatchers.setMain(dispatcher)

    @Test
    fun `initial state`() = processorTest(
        processor = { viewModel.processor },
        thenStates = { assertLast(DayDetailsState()) }
    )

    private val offsetDateTimeSetListReadings = OffsetDateTime.now()
    private val listReadingsForDay = listOf(
        ReadingsRecord(
            id = 1,
            temperature = 20F,
            humidity = 18,
            dateAndTime = OffsetDateTime.now(),
            isSynced = true
        ),
        ReadingsRecord(
            id = 2,
            temperature = 18F,
            humidity = 21,
            dateAndTime = OffsetDateTime.now(),
            isSynced = true
        )
    )

    @Test
    fun `test Event SetListReadings`() = processorTest(
        processor = { viewModel.processor },
        given = {
            coEvery { repository.getReadingsForDay(offsetDateTimeSetListReadings) } returns listReadingsForDay
        },
        whenEvent = DayDetailsEvent.SetListReadings(offsetDateTimeSetListReadings),
        thenStates = {
            assertLast(DayDetailsState(listReadings = listReadingsForDay))
        }
    )

    @Test
    fun `test Event GoBack`() = processorTest(
        processor = { viewModel.processor },
        whenEvent = DayDetailsEvent.GoBack,
        thenEffects = { assertLast(DayDetailsEffect.GoBack) }
    )

    @After
    fun tearDown() = Dispatchers.resetMain()
}
