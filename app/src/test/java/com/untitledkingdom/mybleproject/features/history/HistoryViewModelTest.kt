package com.untitledkingdom.mybleproject.features.history

import com.tomcz.ellipse.test.processorTest
import com.untitledkingdom.mybleproject.database.data.DayReadingsRecord
import com.untitledkingdom.mybleproject.features.main.statehistory.HistoryEffect
import com.untitledkingdom.mybleproject.features.main.statehistory.HistoryEvent
import com.untitledkingdom.mybleproject.features.main.statehistory.HistoryState
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertTrue
import java.time.OffsetDateTime

@FlowPreview
@ExperimentalCoroutinesApi
internal class HistoryViewModelTest {
    private val repository by lazy { mockk<HistoryRepository>() }
    private val dispatcher = StandardTestDispatcher()
    private val viewModel: HistoryViewModel by lazy {
        HistoryViewModel(repository = repository)
    }
    private val listReadingsForDay = listOf(
        DayReadingsRecord(
            minTemperature = 1F,
            averageTemperature = 20F,
            maxTemperature = 18F,
            minHumidity = 1,
            averageHumidity = 20,
            maxHumidity = 18,
            dateAndTime = OffsetDateTime.now(),
        ),
        DayReadingsRecord(
            minTemperature = 4F,
            averageTemperature = 11F,
            maxTemperature = 28F,
            minHumidity = 1,
            averageHumidity = 21,
            maxHumidity = 18,
            dateAndTime = OffsetDateTime.now(),
        ),
    )

    @Before
    fun setup() {
        Dispatchers.setMain(dispatcher)
    }

    @Test
    fun `initial state`() = processorTest(
        processor = { viewModel.processor },
        given = {
            coEvery { repository.getAllDaysWithInfo() } returns listReadingsForDay
        },
        thenStates = {
            assertLast(HistoryState(listDaysWithInfo = listReadingsForDay))
        }
    )

    @Test
    fun `test Event GoBack`() = processorTest(
        processor = { viewModel.processor },
        given = {
            coEvery { repository.getAllDaysWithInfo() } returns listReadingsForDay
        },
        whenEvent = HistoryEvent.GoBack,
        thenEffects = { assertLast(HistoryEffect.GoBack) }
    )

    @Test
    fun `test Event GoToDayDetailsScreen`() = processorTest(
        processor = { viewModel.processor },
        given = {
            coEvery { repository.getAllDaysWithInfo() } returns listReadingsForDay
        },
        whenEvent = HistoryEvent.GoToDayDetailsScreen(OffsetDateTime.now()),
        thenEffects = {
            assertTrue(values.last() is HistoryEffect.GoToDayDetailsScreen)
        }
    )

    @After
    fun tearDown() = Dispatchers.resetMain()
}
