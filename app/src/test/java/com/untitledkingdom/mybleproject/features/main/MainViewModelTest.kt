package com.untitledkingdom.mybleproject.features.main

import com.juul.kable.State
import com.tomcz.ellipse.test.processorTest
import com.untitledkingdom.mybleproject.bluetooth.CustomBluetoothManager
import com.untitledkingdom.mybleproject.bluetooth.data.ReadingsData
import com.untitledkingdom.mybleproject.datastore.DataStorage
import com.untitledkingdom.mybleproject.features.main.state.MainEffect
import com.untitledkingdom.mybleproject.features.main.state.MainEvent
import com.untitledkingdom.mybleproject.features.main.state.MainState
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertTrue

@FlowPreview
@ExperimentalCoroutinesApi
internal class MainViewModelTest {
    private val dataStorage by lazy { mockk<DataStorage>() }
    private val customBluetoothManager by lazy { mockk<CustomBluetoothManager>(relaxed = false) }
    private val dispatcher = StandardTestDispatcher()

    private val viewModel: MainViewModel by lazy {
        MainViewModel(
            dataStorage = dataStorage,
            customBluetoothManager = customBluetoothManager,
        )
    }
    private val defaultDeviceName = "defaultDeviceName"
    private val defaultDeviceAddress = "defaultDeviceAddress"
    private val defaultRssi = 50
    private val valueFlowTemperature = 20F
    private val valueFlowHumidity = 18
    private val defaultBatteryLevel: Short = 100
    private val valueFlowBatteryLevel: Short = 90

    @Before
    fun setup() = Dispatchers.setMain(dispatcher)

    @Test
    fun `initial state`() = processorTest(
        processor = { viewModel.processor },
        given = {
            coEvery { customBluetoothManager.stateFlow } returns MutableStateFlow(State.Connected)
            coEvery { customBluetoothManager.flowReadings } returns flowOf(
                ReadingsData(
                    temperature = valueFlowTemperature,
                    humidity = valueFlowHumidity
                )
            )
            coEvery { customBluetoothManager.flowBatteryLevel } returns flowOf(valueFlowBatteryLevel)
            coEvery { customBluetoothManager.getDeviceName() } returns defaultDeviceName
            coEvery { customBluetoothManager.deviceAddress } returns defaultDeviceAddress
            coEvery { customBluetoothManager.getRssi() } returns defaultRssi
            coEvery { customBluetoothManager.readBattery() } returns defaultBatteryLevel
        },
        thenStates = {
            assertLast(
                MainState(
                    deviceName = defaultDeviceName,
                    deviceAddress = defaultDeviceAddress,
                    humidity = valueFlowHumidity,
                    temperature = valueFlowTemperature,
                    signalStrength = defaultRssi,
                    batteryLevel = valueFlowBatteryLevel
                )
            )
        }
    )

    @Test
    fun `test Event GoBack`() = processorTest(
        processor = { viewModel.processor },
        given = {
            coEvery { customBluetoothManager.stateFlow } returns MutableStateFlow(State.Connected)
            coEvery { customBluetoothManager.flowReadings } returns flowOf(
                ReadingsData(
                    temperature = valueFlowTemperature,
                    humidity = valueFlowHumidity
                )
            )
            coEvery { customBluetoothManager.flowBatteryLevel } returns flowOf(valueFlowBatteryLevel)
            coEvery { customBluetoothManager.getDeviceName() } returns defaultDeviceName
            coEvery { customBluetoothManager.deviceAddress } returns defaultDeviceAddress
            coEvery { customBluetoothManager.getRssi() } returns defaultRssi
            coEvery { customBluetoothManager.readBattery() } returns defaultBatteryLevel
            coEvery { dataStorage.saveToStorage(any(), any()) } returns Unit
            coEvery { customBluetoothManager.disconnect() } returns Unit
        },
        whenEvent = MainEvent.GoBack,
        thenEffects = { assertLast(MainEffect.GoBack) }
    )

    @Test
    fun `test Event GoToHistoryScreen`() = processorTest(
        processor = { viewModel.processor },
        given = {
            coEvery { customBluetoothManager.stateFlow } returns MutableStateFlow(State.Connected)
            coEvery { customBluetoothManager.flowReadings } returns flowOf(
                ReadingsData(
                    temperature = valueFlowTemperature,
                    humidity = valueFlowHumidity
                )
            )
            coEvery { customBluetoothManager.flowBatteryLevel } returns flowOf(valueFlowBatteryLevel)
            coEvery { customBluetoothManager.getDeviceName() } returns defaultDeviceName
            coEvery { customBluetoothManager.deviceAddress } returns defaultDeviceAddress
            coEvery { customBluetoothManager.getRssi() } returns defaultRssi
            coEvery { customBluetoothManager.readBattery() } returns defaultBatteryLevel
            coEvery { dataStorage.saveToStorage(any(), any()) } returns Unit
        },
        whenEvent = MainEvent.GoToHistoryScreen,
        thenEffects = { assertLast(MainEffect.GoToHistoryScreen) }
    )

    @Test
    fun `test Event GoToSettingsScreen`() = processorTest(
        processor = { viewModel.processor },
        given = {
            coEvery { customBluetoothManager.stateFlow } returns MutableStateFlow(State.Connected)
            coEvery { customBluetoothManager.flowReadings } returns flowOf(
                ReadingsData(
                    temperature = valueFlowTemperature,
                    humidity = valueFlowHumidity
                )
            )
            coEvery { customBluetoothManager.flowBatteryLevel } returns flowOf(valueFlowBatteryLevel)
            coEvery { customBluetoothManager.getDeviceName() } returns defaultDeviceName
            coEvery { customBluetoothManager.deviceAddress } returns defaultDeviceAddress
            coEvery { customBluetoothManager.getRssi() } returns defaultRssi
            coEvery { customBluetoothManager.readBattery() } returns defaultBatteryLevel
            coEvery { dataStorage.saveToStorage(any(), any()) } returns Unit
            coEvery { customBluetoothManager.disconnect() } returns Unit
        },
        whenEvent = MainEvent.GoToSettingsScreen,
        thenEffects = { assertLast(MainEffect.GoToSettingsScreen) }
    )

    @Test
    fun `test Event GoToCurrentDay`() = processorTest(
        processor = { viewModel.processor },
        given = {
            coEvery { customBluetoothManager.stateFlow } returns MutableStateFlow(State.Connected)
            coEvery { customBluetoothManager.flowReadings } returns flowOf(
                ReadingsData(
                    temperature = valueFlowTemperature,
                    humidity = valueFlowHumidity
                )
            )
            coEvery { customBluetoothManager.flowBatteryLevel } returns flowOf(valueFlowBatteryLevel)
            coEvery { customBluetoothManager.getDeviceName() } returns defaultDeviceName
            coEvery { customBluetoothManager.deviceAddress } returns defaultDeviceAddress
            coEvery { customBluetoothManager.getRssi() } returns defaultRssi
            coEvery { customBluetoothManager.readBattery() } returns defaultBatteryLevel
            coEvery { dataStorage.saveToStorage(any(), any()) } returns Unit
            coEvery { customBluetoothManager.disconnect() } returns Unit
        },
        whenEvent = MainEvent.GoToCurrentDay,
        thenEffects = {
            assertTrue(values.last() is MainEffect.GoToDayDetailsScreen)
        }
    )

    @After
    fun tearDown() = Dispatchers.resetMain()
}
