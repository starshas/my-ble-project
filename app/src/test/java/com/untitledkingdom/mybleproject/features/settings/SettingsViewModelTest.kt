package com.untitledkingdom.mybleproject.features.settings

import com.tomcz.ellipse.test.processorTest
import com.untitledkingdom.mybleproject.features.settings.state.SettingsEffect
import com.untitledkingdom.mybleproject.features.settings.state.SettingsEvent
import com.untitledkingdom.mybleproject.features.settings.state.SettingsState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test

@FlowPreview
@ExperimentalCoroutinesApi
internal class SettingsViewModelTest {
    private val dispatcher = StandardTestDispatcher()

    private val viewModel: SettingsViewModel by lazy {
        SettingsViewModel()
    }

    @Before
    fun setup() = Dispatchers.setMain(dispatcher)

    @Test
    fun `initial state`() = processorTest(
        processor = { viewModel.processor },
        thenStates = { assertLast(SettingsState) }
    )

    @Test
    fun `test Event GoToSelectDeviceScreen`() = processorTest(
        processor = { viewModel.processor },
        whenEvent = SettingsEvent.GoToSelectDeviceScreen,
        thenEffects = { assertLast(SettingsEffect.GoToSelectDeviceScreen) }
    )

    @After
    fun tearDown() = Dispatchers.resetMain()
}
