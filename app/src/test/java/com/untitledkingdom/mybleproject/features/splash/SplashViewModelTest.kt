package com.untitledkingdom.mybleproject.features.splash

import com.tomcz.ellipse.test.processorTest
import com.untitledkingdom.mybleproject.bluetooth.CustomBluetoothManager
import com.untitledkingdom.mybleproject.datastore.DataStorage
import com.untitledkingdom.mybleproject.features.splash.state.SplashEffect
import com.untitledkingdom.mybleproject.features.splash.state.SplashEvent
import com.untitledkingdom.mybleproject.features.splash.state.SplashState
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
internal class SplashViewModelTest {
    private val dataStorage by lazy { mockk<DataStorage>() }
    private val customBluetoothManager by lazy { mockk<CustomBluetoothManager>(relaxed = false) }
    private val dispatcher = StandardTestDispatcher()
    private val viewModel: SplashViewModel by lazy {
        SplashViewModel(
            dataStorage = dataStorage,
            customBluetoothManager = customBluetoothManager
        )
    }
    private val savedDeviceMac = "savedDeviceMac"

    @Before
    fun setup() = Dispatchers.setMain(dispatcher)

    @Test
    fun `initial state`() = processorTest(
        processor = { viewModel.processor },
        given = {
            coEvery { dataStorage.getFromStorage(any()) } returns ""
        },
        thenStates = { assertLast(SplashState) }
    )

    @Test
    fun `when no saved device then effect GoToWelcome`() = processorTest(
        processor = { viewModel.processor },
        given = {
            coEvery { dataStorage.getFromStorage(any()) } returns ""
            coEvery { customBluetoothManager.disconnect() } returns Unit
            coEvery { customBluetoothManager.isConnected } returns false
        },
        whenEvent = SplashEvent.GoToNextScreen,
        thenEffects = { assertLast(SplashEffect.GoToWelcomeScreen) }
    )

    @Test
    fun `when connected to saved device then effect GoToMain`() = processorTest(
        processor = { viewModel.processor },
        given = {
            coEvery { dataStorage.getFromStorage(any()) } returns savedDeviceMac
            coEvery {
                customBluetoothManager.connect(
                    scope = any(),
                    identifier = any(),
                    callbackConnected = any(),
                    callbackError = any()
                )
            } returns Unit
            coEvery { customBluetoothManager.disconnect() } returns Unit
            coEvery { customBluetoothManager.isConnected } returns true
        },
        whenEvent = SplashEvent.GoToNextScreen,
        thenEffects = { assertLast(SplashEffect.GoToMainScreen) }
    )

    @After
    fun tearDown() = Dispatchers.resetMain()
}
