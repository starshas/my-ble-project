package com.untitledkingdom.mybleproject.features.welcome

import com.juul.kable.Advertisement
import com.tomcz.ellipse.test.processorTest
import com.untitledkingdom.mybleproject.R
import com.untitledkingdom.mybleproject.bluetooth.CustomBluetoothManager
import com.untitledkingdom.mybleproject.datastore.DataStorage
import com.untitledkingdom.mybleproject.features.welcome.state.WelcomeEffect
import com.untitledkingdom.mybleproject.features.welcome.state.WelcomeEvent
import com.untitledkingdom.mybleproject.features.welcome.state.WelcomeState
import com.untitledkingdom.mybleproject.utils.getNameOrAddress
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkClass
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test

@FlowPreview
@ExperimentalCoroutinesApi
internal class WelcomeViewModelTest {
    private val dataStorage by lazy { mockk<DataStorage>() }
    private val repository by lazy { mockk<WelcomeRepository>() }
    private val customBluetoothManager by lazy { mockk<CustomBluetoothManager>(relaxed = false) }
    private val dispatcher = StandardTestDispatcher()
    private val scannedAdvertisements = arrayOf(
        mockkClass(Advertisement::class, relaxed = true)
            .apply { coEvery { this@apply.address } answers { this.hashCode().toString() } },
        mockkClass(Advertisement::class, relaxed = true)
            .apply { coEvery { this@apply.address } answers { this.hashCode().toString() } }
    )
    private val viewModel: WelcomeViewModel by lazy {
        WelcomeViewModel(
            dataStorage = dataStorage,
            customBluetoothManager = customBluetoothManager,
            repository = repository
        )
    }
    private val advertisementConnectTo = mockkClass(Advertisement::class, relaxed = true)

    @Before
    fun setup() = Dispatchers.setMain(dispatcher)

    @Test
    fun `initial state`() = processorTest(
        processor = { viewModel.processor },
        thenStates = { assertLast(WelcomeState()) }
    )

    @Test
    fun `when BluetoothTurnedOn then add some devices`() = processorTest(
        processor = { viewModel.processor },
        given = {
            coEvery { repository.getAdvertisements() } returns flowOf(
                *scannedAdvertisements
            )
        },
        whenEvent = WelcomeEvent.BluetoothTurnedOn,
        thenStates = {
            assertLast(WelcomeState(listAdvertisements = scannedAdvertisements.toList()))
        }
    )

    @Test
    fun `when event ConnectToDevice and currently connecting`() = processorTest(
        processor = { viewModel.processor },
        given = {
            every { customBluetoothManager.isConnecting } returns true
            coEvery {
                customBluetoothManager.connect(
                    scope = any(),
                    advertisement = any(),
                    callbackConnected = any(),
                    callbackError = any()
                )
            } returns Unit
            coEvery { customBluetoothManager.disconnect() } returns Unit
            coEvery { dataStorage.saveToStorage(any(), any()) } returns Unit
        },
        whenEvent = WelcomeEvent.ConnectToDevice(advertisementConnectTo),
        thenEffects = {
            assertValues(
                WelcomeEffect.ShowToast(
                    resId = R.string.welcome_disconnecting,
                ),
                WelcomeEffect.ShowToast(
                    resId = R.string.welcome_disconnected,
                ),
                WelcomeEffect.ShowToast(
                    resId = R.string.welcome_connecting_to,
                    parameterString = advertisementConnectTo.getNameOrAddress()
                )
            )
        }
    )

    @Test
    fun `when event ConnectToDevice and currently not connecting`() = processorTest(
        processor = { viewModel.processor },
        given = {
            every { customBluetoothManager.isConnecting } returns false
            coEvery {
                customBluetoothManager.connect(
                    scope = any(),
                    advertisement = any(),
                    callbackConnected = any(),
                    callbackError = any()
                )
            } returns Unit
            coEvery { dataStorage.saveToStorage(any(), any()) } returns Unit
        },
        whenEvent = WelcomeEvent.ConnectToDevice(advertisementConnectTo),
        thenEffects = {
            assertValues(
                WelcomeEffect.ShowToast(
                    resId = R.string.welcome_connecting_to,
                    parameterString = advertisementConnectTo.getNameOrAddress()
                )
            )
        }
    )

    @After
    fun tearDown() = Dispatchers.resetMain()
}
